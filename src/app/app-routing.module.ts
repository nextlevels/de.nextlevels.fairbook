import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/auth/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'signup',
    loadChildren: () => import('./pages/auth/signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'erfassen1',
    loadChildren: () => import('./pages/erfassen1/erfassen1.module').then( m => m.Erfassen1PageModule)
  },
  {
    path: 'erfassen2',
    loadChildren: () => import('./pages/erfassen2/erfassen2.module').then( m => m.Erfassen2PageModule)
  },
  {
    path: 'erfassen-austeller',
    loadChildren: () => import('./pages/erfassen-austeller/erfassen-austeller.module').then( m => m.ErfassenAustellerPageModule)
  },
  {
    path: 'add-page',
    loadChildren: () => import('./pages/add-page/add-page.module').then( m => m.AddPagePageModule)
  },
  {
    path: 'add-note-page',
    loadChildren: () => import('./pages/add-note-page/add-note-page.module').then( m => m.AddNotePagePageModule)
  },
  {
    path: 'add-textnote-page',
    loadChildren: () => import('./pages/add-textnote-page/add-textnote-page.module').then( m => m.AddTextnotePagePageModule)
  },
  {
    path: 'add-audio-note-page',
    loadChildren: () => import('./pages/add-audio-note-page/add-audio-note-page.module').then( m => m.AddAudioNotePagePageModule)
  },
  {
    path: 'anfragen-seite',
    loadChildren: () => import('./pages/anfragen-seite/anfragen-seite.module').then( m => m.AnfragenSeitePageModule)
  },
  {
    path: 'add-product',
    loadChildren: () => import('./pages/add-product/add-product.module').then( m => m.AddProductPageModule)
  },
  {
    path: 'exhibtioner1',
    loadChildren: () => import('./pages/exhibtioner1/exhibtioner1.module').then( m => m.Exhibtioner1PageModule)
  },
  {
    path: 'note-page',
    loadChildren: () => import('./pages/note-page/note-page.module').then( m => m.NotePagePageModule)
  },
  {
    path: 'adjustments',
    loadChildren: () => import('./pages/adjustments/adjustments.module').then( m => m.AdjustmentsPageModule)
  },
  {
    path: 'note-sel',
    loadChildren: () => import('./pages/note-sel/note-sel.module').then( m => m.NoteSelPageModule)
  },
  {
    path: 'audionote2',
    loadChildren: () => import('./pages/audionote2/audionote2.module').then( m => m.Audionote2PageModule)
  },
  {
    path: 'audionote3',
    loadChildren: () => import('./pages/audionote3/audionote3.module').then( m => m.Audionote3PageModule)
  },
  {
    path: 'profil',
    loadChildren: () => import('./pages/auth/profil/profil.module').then( m => m.ProfilPageModule)
  },
  {
    path: 'bl-katalog',
    loadChildren: () => import('./pages/bl-katalog/bl-katalog.module').then( m => m.BlKatalogPageModule)
  },
  {
    path: 'notes-mixed',
    loadChildren: () => import('./pages/notes-mixed/notes-mixed.module').then( m => m.NotesMixedPageModule)
  },
  {
    path: 'exhibitors-list',
    loadChildren: () => import('./pages/exhibitors-list/exhibitors-list.module').then( m => m.ExhibitorsListPageModule)
  },
  {
    path: 'productadd',
    loadChildren: () => import('./productadd/productadd.module').then( m => m.ProductaddPageModule)
  },
  {
    path: 'social-sharing',
    loadChildren: () => import('./social-sharing/social-sharing.module').then( m => m.SocialSharingPageModule)
  },
  {
    path: 'messebucher',
    loadChildren: () => import('./messebucher/messebucher.module').then( m => m.MessebucherPageModule)
  },
  {
    path: 'messebucher1',
    loadChildren: () => import('./messebucher1/messebucher1.module').then( m => m.Messebucher1PageModule)
  },
  {
    path: 'passwort-vergessen',
    loadChildren: () => import('./pages/auth/passwort-vergessen/passwort-vergessen.module').then( m => m.PasswortVergessenPageModule)
  },
  {
    path: 'productadd1',
    loadChildren: () => import('./productadd1/productadd1.module').then( m => m.Productadd1PageModule)
  },

  {
    path: 'notebooks',
    loadChildren: () => import('./pages/notebooks/notebooks.module').then( m => m.NotebooksPageModule)
  },
  {
    path: 'notebook/:id',
    loadChildren: () => import('./pages/notebook/notebook.module').then( m => m.NotebookPageModule)
  },
  {
    path: 'note/:id',
    loadChildren: () => import('./pages/note/note.module').then( m => m.NotePageModule)
  },
  {
    path: 'exhibitors-detail/:id',
    loadChildren: () => import('./pages/exhibitors-detail/exhibitors-detail.module').then( m => m.ExhibitorsDetailPageModule)
  },
  {
    path: 'exhibition-info',
    loadChildren: () => import('./pages/exhibition-info/exhibition-info.module').then( m => m.ExhibitionInfoPageModule)
  }


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
