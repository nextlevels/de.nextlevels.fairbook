import { Component, ViewChild } from '@angular/core';

import { IonRouterOutlet, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { StorageService } from './services/storage/storage.service';
import { ApiService } from './services/api-service/api.service';
import { DataService } from './services/data/data.service';
import { Principal } from './models/user.model';
import { Location } from '@angular/common';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { LanguageService } from './services/language.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  @ViewChild(IonRouterOutlet, { static : true }) routerOutlet: IonRouterOutlet;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storageService: StorageService,
    private apiService: ApiService,
    private router: Router,
    private location: Location,
    private screenOrientation: ScreenOrientation,
    private languageService: LanguageService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    if (this.platform.is('cordova')) {
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
    // use stored token for silent refresh token operation
    this.storageService.get('fairbook_token').then(token => {
      if (token) {  // if token exists - try to update
        let currentPrincipal = DataService.principal.value;
        if (!currentPrincipal) {
          currentPrincipal = new Principal({
            token,
            user: null
          });
        }
        DataService.principal.next(currentPrincipal);
        this.apiService.updateToken().then(principal => {
          DataService.principal.next(principal);
        });

        this.storageService.get('last_notebook_id').then(notebookID => {
          if (notebookID){
          this.apiService.getNotebook(notebookID).then(data => {
              DataService.noteBook.next(data);
          });
          }else {
            this.router.navigate(['/notebooks']);
          }
        });
      } else {  // else redirect to login page
        this.router.navigate(['/login']);
      }
    });
    // when principal is changed - request app user data.
    DataService.principal.subscribe(principal => {
      if (principal && principal.token) {
        this.apiService.getAppUser().then(appUser => {
          DataService.appUser.next(appUser);
        });
        const token = principal ? principal.token : '';
        this.storageService.get('autoLogin').then(autoLogin => {
          console.log('Auto login: ' + autoLogin);
          if (autoLogin === 'true') {
            this.storageService.set('fairbook_token', token).then(() => {});
          } else {
            this.storageService.delete('fairbook_token').then(() => {});
          }
        });
      }
    });
    // platform specific subscriptions
    this.platform.ready().then(() => {
      this.routerOutlet.swipeGesture = false;   // prevent IOS swipe
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.languageService.setInitialAppLanguage();
      // handle back button event
      this.platform.backButton.subscribe(() => {
        this.location.back();
      });
    });
  }
}

