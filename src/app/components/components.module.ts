import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { RequiredComponent } from './util/required/required.component';
import { MenuComponent } from './modal/menu/menu.component';
import { NotebookFormComponent } from './form/notebook-form/notebook-form.component';
import { NoteFormComponent } from './form/note-form/note-form.component';
import { SpinnerComponent } from './util/spinner/spinner.component';
import { NoteCardComponent } from './widget/note-card/note-card.component';
import { ImageControlComponent } from './control/image-control/image-control.component';
import { AudioControlComponent } from './control/audio-control/audio-control.component';
import { NotesTileViewComponent } from './widget/notes-tile-view/notes-tile-view.component';
import { ToolbarWithSlotsComponent } from './toolbar-with-slots/toolbar-with-slots.component';
import { FooterComponent } from './footer/footer.component';
import { MesseControlComponent } from './control/messe-control/messe-control.component';
import { MesseSearchComponent } from './widget/messe-search/messe-search.component';
import { TranslateModule } from '@ngx-translate/core';
import { LanguagePopoverComponent } from './language-popover/language-popover.component';
import { PipeModule } from '../pipe.module';
import { UserSearchComponent } from './widget/user-search/user-search.component';
import { UsersControlComponent } from './control/users-control/users-control.component';


@NgModule({
    declarations: [
        ToolbarComponent,
        ToolbarWithSlotsComponent,
        FooterComponent,
        RequiredComponent,
        MenuComponent,
        NotebookFormComponent,
        NoteFormComponent,
        SpinnerComponent,
        NoteCardComponent,
        ImageControlComponent,
        AudioControlComponent,
        NotesTileViewComponent,
        MesseControlComponent,
        MesseSearchComponent,
        LanguagePopoverComponent,
        UserSearchComponent,
        UsersControlComponent
    ],
    imports: [CommonModule, IonicModule, ReactiveFormsModule, TranslateModule, PipeModule.forRoot()],
    exports: [
        ToolbarComponent,
        ToolbarWithSlotsComponent,
        FooterComponent,
        RequiredComponent,
        MenuComponent,
        NotebookFormComponent,
        NoteFormComponent,
        SpinnerComponent,
        NoteCardComponent,
        ImageControlComponent,
        AudioControlComponent,
        NotesTileViewComponent,
        MesseControlComponent,
        MesseSearchComponent,
        UserSearchComponent,
        UsersControlComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ComponentsModule {
}
