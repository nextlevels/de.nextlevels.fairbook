import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Attachment } from 'src/app/models/attachment.model';
import { MediaCapture, MediaFile, CaptureError, CaptureAudioOptions } from '@ionic-native/media-capture/ngx';
import { FileService } from 'src/app/services/file-service/file.service';
import { SharedService } from 'src/app/services/shared/shared.service';

@Component({
  selector: 'app-audio-control',
  templateUrl: './audio-control.component.html',
  styleUrls: ['./audio-control.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => AudioControlComponent),
    multi: true
  }]
})
export class AudioControlComponent implements OnInit, ControlValueAccessor {

  @Input() value: Attachment
  @Input() color: string;

  fn: any = null;
  fnTouched: any = null;

  constructor(
    private mediaCapture: MediaCapture,
    private fileService: FileService,
    private sharedService: SharedService
  ) { }

  ngOnInit() { }

  writeValue(obj: any): void {
    this.value = obj;
  }
  registerOnChange(fn: any): void {
    this.fn = fn;
  }
  registerOnTouched(fn: any): void {
    this.fnTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
  }

  recordAudio() {
    this.fnTouched(true);
    let options: CaptureAudioOptions = { limit: 1}
    this.mediaCapture.captureAudio(options).then(
      (data: MediaFile[]) => {
        console.log("DATA: " + JSON.stringify(data));
        const audio = data[0];
        console.log("Audio: " + JSON.stringify(audio));
        this.fileService.readURI(audio.fullPath).then(d => {
          console.log("Base64: " + JSON.stringify(d));
          this.value = new Attachment({
            data: d,
            file_name: audio.name,
            size: audio.size,
            content_type: audio.type
          });
          console.log(JSON.stringify(this.value));
          this.fn(this.value);
        });
      },
      (err: CaptureError) => {
        if (err.code == '4') {
          this.sharedService.simpleAlert('Fehler',
            'Mikrofon Zugriff verweigert. Bitte aktivieren Sie den Mikrofon Zugriff, bevor Sie diese Funktion verwenden.');
        }
        console.error(JSON.stringify(err));
        
      }
    );
  }

  removeAudio(event) {
    event.stopPropagation();
    this.value = null;
    this.fn(this.value);
    this.fnTouched(true);
  }

}
