import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Attachment } from 'src/app/models/attachment.model';
import { FileService } from 'src/app/services/file-service/file.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-image-control',
  templateUrl: './image-control.component.html',
  styleUrls: ['./image-control.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => ImageControlComponent),
    multi: true
  }]
})
export class ImageControlComponent implements OnInit, ControlValueAccessor {

  @Input() value: Attachment
  @Input() color: string;

  fn: any = null;
  fnTouched: any = null;

  constructor(
    private fileService: FileService,
    private camera: Camera
  ) { }

  ngOnInit() { }

  writeValue(obj: any): void {
    this.value = obj;
  }
  registerOnChange(fn: any): void {
    this.fn = fn;
  }
  registerOnTouched(fn: any): void {
    this.fnTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
  }

  selectImage() {
    this.fnTouched(true);
    this.fileService.selectFileFromDevice().then(file => {
      this.value = file;
      this.fn(this.value);
    });
  }

  removeImage(event) {
    event.stopPropagation();
    this.value = null;
    this.fn(this.value);
    this.fnTouched(true);
  }

  makePhoto(event) {
    event.stopPropagation();
    this.fnTouched(true);
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.value = new Attachment({
        data: base64Image,
        file_name: 'camera-photo-' + new Date().getTime() + '.jpg'
      });
      this.fn(this.value);
    }, (err) => {
      console.error(JSON.stringify(err));
    });
  }

}
