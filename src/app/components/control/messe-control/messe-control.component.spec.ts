import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MesseControlComponent } from './messe-control.component';

describe('MesseControlComponent', () => {
  let component: MesseControlComponent;
  let fixture: ComponentFixture<MesseControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesseControlComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MesseControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
