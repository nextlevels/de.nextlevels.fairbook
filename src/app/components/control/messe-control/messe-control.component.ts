import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Exhibition } from 'src/app/models/exhibition.model';
import { MesseSearchComponent } from '../../widget/messe-search/messe-search.component';

@Component({
  selector: 'app-messe-control',
  templateUrl: './messe-control.component.html',
  styleUrls: ['./messe-control.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => MesseControlComponent),
    multi: true
  }]
})
export class MesseControlComponent implements OnInit, ControlValueAccessor {
  @Input() value: Exhibition;
  @Input() color: string;

  fn: any = null;
  fnTouched: any = null;

  constructor(
    private modalController: ModalController
  ) { }

  ngOnInit() { }

  writeValue(obj: any): void {
    this.value = obj;
  }
  registerOnChange(fn: any): void {
    this.fn = fn;
  }
  registerOnTouched(fn: any): void {
    this.fnTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
  }

  clearInput() {
    this.value = null;
    this.fn(this.value);
    this.fnTouched(true);
  }

  async openSearch() {
    const modal = await this.modalController.create({
      component: MesseSearchComponent,
      componentProps: {
        onSelect: (exh: Exhibition) => {
          this.value = exh;
          this.fn(this.value);
          this.fnTouched(true);
          this.modalController.dismiss();
        }
      }
    });
    await modal.present();
  }

}
