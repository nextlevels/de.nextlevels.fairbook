import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { AppUser } from 'src/app/models/user.model';
import { UserSearchComponent } from '../../widget/user-search/user-search.component';

@Component({
  selector: 'app-users-control',
  templateUrl: './users-control.component.html',
  styleUrls: ['./users-control.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => UsersControlComponent),
    multi: true
  }]
})
export class UsersControlComponent implements OnInit, ControlValueAccessor {
  @Input() value: AppUser[] = [];

  fn: any = null;
  fnTouched: any = null;

  constructor(
    private modalController: ModalController
  ) { }

  ngOnInit() { }

  writeValue(obj: any): void {
    this.value = obj;
  }
  registerOnChange(fn: any): void {
    this.fn = fn;
  }
  registerOnTouched(fn: any): void {
    this.fnTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
  }

  onRemoveParticipant(user: AppUser) {
    this.value = this.value.filter(u => u.id !== user.id);
    this.fn(this.value);
    this.fnTouched(true);
  }

  async openSearch() {
    const modal = await this.modalController.create({
      component: UserSearchComponent,
      componentProps: {
        onSelect: (users: AppUser[]) => {
          this.value = users;
          this.fn(this.value);
          this.fnTouched(true);
          this.modalController.dismiss();
        },
        selected: this.value
      }
    });
    await modal.present();
  }
}
