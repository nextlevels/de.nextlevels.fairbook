import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { Note, TYPES, TYPE_AUDIO, TYPE_IMAGE, TYPE_TEXT } from 'src/app/models/note.model';
import { Notebook } from 'src/app/models/notebook.model';
import { ApiService } from 'src/app/services/api-service/api.service';

@Component({
  selector: 'app-note-form',
  templateUrl: './note-form.component.html',
  styleUrls: ['./note-form.component.scss'],
})
export class NoteFormComponent implements OnInit {

  @Input() afterSubmit: Function;
  @Input() note: Note;
  @Input() notebook: Notebook;
  @Input() showProductFields: boolean = false

  form: FormGroup;

  constructor(
    private modalController: ModalController,
    private formBuilder: FormBuilder,
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.initForm();
    if (this.notebook) {
      this.form.patchValue({
        notebook_id: this.notebook.id
      });
    }
    this.form.get('type').valueChanges.subscribe(type => {
      this.form.controls.content.setValidators(type === TYPE_TEXT.value ? [Validators.required, Validators.maxLength(65535)] : []);
      this.form.controls.content.updateValueAndValidity();
      this.form.controls.file.setValidators(type === TYPE_IMAGE.value || type === TYPE_AUDIO.value ? [Validators.required] : []);
      this.form.controls.file.updateValueAndValidity();
    });
    if (this.note) {
      this.form.patchValue({
        id: this.note.id,
        title: this.note.title,
        content: this.note.content,
        file: this.note.file,
        type: this.note.type,
        notebook_id: this.note.notebook_id,
        product_name: this.note.product_name,
        article_id: this.note.article_id
      });
    }
  }

  close() {
    this.modalController.dismiss();
  }

  color(control: string) {
    return this.form.get(control).touched ? (this.form.get(control).errors ? 'danger' : 'success') : '';
  }

  types() {
    return TYPES;
  }

  isTypeText() {
    return TYPE_TEXT.value === this.form.get('type').value;
  }

  isTypeImage() {
    return TYPE_IMAGE.value === this.form.get('type').value;
  }

  isTypeAudio() {
    return TYPE_AUDIO.value === this.form.get('type').value;
  }

  submit() {
    const data = this.form.value;
    if (data.id) {
      this.apiService.updateNote(data).then(() => {
        this.close();
        if (this.afterSubmit) {
          this.afterSubmit();
        }
      });
    } else {
      this.apiService.createNote(data).then(() => {
        this.close();
        if (this.afterSubmit) {
          this.afterSubmit();
        }
      });
    }
  }

  private initForm() {
    this.form = this.formBuilder.group({
      id: new FormControl(null, []),
      title: new FormControl(null, [Validators.required, Validators.maxLength(65535)]),
      content: new FormControl(null, [Validators.required, Validators.maxLength(65535)]),
      file: new FormControl(null, [Validators.required]),
      type: new FormControl(null, [Validators.required]),
      notebook_id: new FormControl(null, [Validators.required]),
      product_name: new FormControl(null, [Validators.maxLength(255)]),
      article_id: new FormControl(null, [Validators.maxLength(255)])
    });
  }

}
