import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, ModalController } from '@ionic/angular';
import { Exhibition } from 'src/app/models/exhibition.model';
import { Notebook } from 'src/app/models/notebook.model';
import { ApiService } from 'src/app/services/api-service/api.service';
import { DataService } from 'src/app/services/data/data.service';
import { StorageService } from 'src/app/services/storage/storage.service';
import {TranslateService} from "@ngx-translate/core";
import { AppUser } from 'src/app/models/user.model';

@Component({
  selector: 'app-notebook-form',
  templateUrl: './notebook-form.component.html',
  styleUrls: ['./notebook-form.component.scss'],
})
export class NotebookFormComponent implements OnInit {

  @Input() afterSubmit: Function;
  @Input() afterDelete: Function;
  @Input() notebook: Notebook;

  appUser: AppUser;

  form: FormGroup

  withoutExhibitionView = false;

  constructor(
    private modalController: ModalController,
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private alertController: AlertController,
    private router: Router,
    private storageService: StorageService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    DataService.appUser.subscribe(u => {
      this.appUser = u;
    })
    this.initForm();
    if (this.notebook) {
      this.form.patchValue({
        id: this.notebook.id,
        title: this.notebook.title,
        description: this.notebook.description,
        exhibition_id: this.notebook.exhibition,
        is_shared: this.notebook.is_shared,
        participants: this.notebook.participants
      });
    }
  }

  close() {
    this.modalController.dismiss();
  }

  color(control: string) {
    return this.form.get(control).touched ? (this.form.get(control).errors ? 'danger' : 'success') : '';
  }

  isEditable() {
    return this.appUser && this.notebook && this.appUser.id === this.notebook.owner_id;
  }

  createNotebook() {
    const formData = this.form.value;
    if (formData.exhibition_id && formData.exhibition_id.id) {
      formData.exhibition_id = formData.exhibition_id.id;
    }
    if (this.notebook) {
      this.apiService.updateNotebook(formData).then(() => {
        this.close();
        this.apiService.getUserNotebooks().then(notebooks => {
          DataService.noteBook.next(notebooks.filter(n => n.id === this.notebook.id)[0]);
        });
        if (this.afterSubmit) {
          this.afterSubmit();
        }
      });
    } else {
      this.apiService.createNotebook(formData).then(() => {
        this.close();
        if (this.afterSubmit) {
          this.afterSubmit();
        }
      });
    }
  }

  switchMode() {
    this.withoutExhibitionView = true;
  }

  private initForm() {
    this.form = this.formBuilder.group({
      id: new FormControl(null, []),
      title: new FormControl(null, [Validators.required, Validators.maxLength(65535)]),
      description: new FormControl(null, [Validators.maxLength(65535)]),
      exhibition_id: new FormControl(null, []),
      is_shared: new FormControl(false, []),
      participants: new FormControl([], [])
    });
  }

  async deleteNotebook() {
    const alert = await this.alertController.create({
      header: 'Bestätigung löschen',
      message: 'Das Löschen des Messebuches kann nicht rückgängig gemacht werden. Dennoch löschen?',
      buttons: [
        {
          text: 'Nein',
          role: 'cancel',
          cssClass: 'secondary'
        }, {
          text: 'Ja',
          handler: () => {
            this.apiService.deleteNotebook(this.notebook.id).then(() => {
              this.close();
              if (this.afterDelete) {
                this.afterDelete();
              }
              this.storageService.delete('last_notebook_id').then(() => {
                DataService.noteBook.next(null);
                this.router.navigate(['notebooks']);
              });
            });
          }
        }
      ]
    });
    await alert.present();
  }

}
