import { Component, OnInit } from '@angular/core';
import { LanguageService } from '../../services/language.service';
import {PopoverController} from "@ionic/angular";

@Component({
  selector: 'app-language-popover',
  templateUrl: './language-popover.component.html',
  styleUrls: ['./language-popover.component.scss'],
})
export class LanguagePopoverComponent implements OnInit {

  languages = [];
  selected = '';

  constructor(private languageService: LanguageService, private popoverCtrl: PopoverController) { }

  ngOnInit() {
    this.languages = this.languageService.getLanguages();
    this.selected = this.languageService.selected;
  }

  select(lng) {
    this.languageService.setLanguage(lng);
    this.popoverCtrl.dismiss();
  }

  close() {
    this.popoverCtrl.dismiss();
  }
}
