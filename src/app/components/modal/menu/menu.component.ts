import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {ModalController, PopoverController} from '@ionic/angular';
import { ProfilPage } from 'src/app/pages/auth/profil/profil.page';
import { DataService } from 'src/app/services/data/data.service';
import {Notebook} from '../../../models/notebook.model';
import {NotebookFormComponent} from '../../form/notebook-form/notebook-form.component';
import { LanguagePopoverComponent } from '../../language-popover/language-popover.component';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

  _notebook: Notebook;

  constructor(
    private modalCtrl: ModalController,
    private router: Router,
    private dataService: DataService,
    private modalController: ModalController,
    public popoverController: PopoverController
  ) { }

  ngOnInit() {
    DataService.noteBook.subscribe(notebook => {
      this._notebook = notebook;
    });
  }

  async presentPopover() {
    const popover = await this.popoverController.create({
      component: LanguagePopoverComponent,
      cssClass: 'popover'
    });
    await popover.present();

    const { role } = await popover.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  close() {
    this.modalCtrl.dismiss();
  }

  href(link) {
    this.close();
    this.router.navigate([link]);
  }

  async editNotebook() {
    const modal = await this.modalController.create({
      component: NotebookFormComponent,
      componentProps: {
        notebook: this._notebook,
        afterSubmit: () => {

        },
        afterDelete: () => {
          setTimeout(() => {
            this.modalController.dismiss();
          }, 1000);
        }
      }
    });
    await modal.present();
  }

  async editProfile() {
    const modal = await this.modalController.create({
      component: ProfilPage,
      componentProps: {

      }
    });
    await modal.present();
  }

  logout() {
    this.close();
    this.dataService.logout();
  }

  dismiss(data?){
      this.modalCtrl.dismiss(data);
  }

}
