import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-toolbar-with-slots',
  templateUrl: './toolbar-with-slots.component.html',
  styleUrls: ['./toolbar-with-slots.component.scss'],
})
export class ToolbarWithSlotsComponent implements OnInit {

  @Input() title;

  constructor() { }

  ngOnInit() {}

}
