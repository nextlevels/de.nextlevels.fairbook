import { AbstractControl, ValidatorFn } from '@angular/forms';

export function stringsMatchValidator(name1, name2): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
        const val1 = control.get(name1).value;
        const val2 = control.get(name2).value;
        return val1 === val2 ? null : { notMatch: true };
    };
}