import { Component, Input, OnInit } from '@angular/core';
import { Exhibition } from 'src/app/models/exhibition.model';
import { ApiService } from 'src/app/services/api-service/api.service';

@Component({
  selector: 'app-messe-search',
  templateUrl: './messe-search.component.html',
  styleUrls: ['./messe-search.component.scss'],
})
export class MesseSearchComponent implements OnInit {

  @Input() onSelect: Function;

  exhibitions: Exhibition[] = [];

  filtered: Exhibition[] = [];

  searchText: string = '';

  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.apiService.getAllExhibitions().then(data => {
      this.exhibitions = data;
      this.filtered = this.exhibitions;
    });
  }

  onFilterChange(event) {
    this.searchText = event.detail.value;
    this.onFilter();
  }

  onFilter() {
    this.filtered = this.exhibitions.filter(e => {
      let passed = true;
      if (this.searchText && e.name.toLowerCase().indexOf(this.searchText.toLowerCase()) === -1) {
        passed = false;
      }
      return passed;
    });
  }

  onSelectExhibition(ex: Exhibition) {
    this.onSelect(ex);
  }

  getName(ex: Exhibition) {
    return this.highlightText(ex.name);
  }

  private highlightText(text: string) {
    const idx = text.toLowerCase().indexOf(this.searchText.toLowerCase());
    if (this.searchText.length > 0 && idx !== -1) {
      return `${text.substring(0, idx)}<b>${text.substring(idx, idx + this.searchText.length)}</b>${text.substring(idx + this.searchText.length)}`;
    } else {
      return text;
    }
  }

}
