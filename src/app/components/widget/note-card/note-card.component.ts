import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Note, TYPE_AUDIO, TYPE_IMAGE, TYPE_TEXT } from 'src/app/models/note.model';

@Component({
  selector: 'app-note-card',
  templateUrl: './note-card.component.html',
  styleUrls: ['./note-card.component.scss'],
})
export class NoteCardComponent implements OnInit {

  @Input() note: Note;
  @Input() count: number;
  @Input() searchToken: string = '';
  @Output() noteClick = new EventEmitter<Note>();

  constructor() { }

  ngOnInit() {
  }

  isTextType() {
    return this.note.type === TYPE_TEXT.value;
  }

  isImageType() {
    return this.note.type === TYPE_IMAGE.value;
  }

  isAudioType() {
    return this.note.type === TYPE_AUDIO.value;
  }

  isProductType() {
    return this.note.product_name ? true : false;
  }


  icon() {
    if (this.isProductType()) {
      return 'pricetags-outline';
    }
    if (this.isAudioType()) {
      return "musical-notes-outline";
    } else if (this.isImageType()) {
      return "camera-outline";
    } else if (this.isTextType()) {
      return "pencil-outline"
    }
  }

  noteClickHandler() {
    const audio = <any>document.querySelector('audio[id="' + this.note.id + '"]');
    if (audio) {
      audio.pause();
    }
    this.noteClick.next(this.note);
  }

  getNoteTitle() {
    return this.highlightText(this.note.title);
  }

  getNoteContent() {
    return this.note.content ? this.highlightText(this.note.content) : '';
  }

  private highlightText(text: string) {
    const idx = text.toLowerCase().indexOf(this.searchToken.toLowerCase());
    if (this.searchToken.length > 0 && idx !== -1) {
      return `${text.substring(0, idx)}<b>${text.substring(idx, idx + this.searchToken.length)}</b>${text.substring(idx + this.searchToken.length)}`;
    } else {
      return text;
    }
  }

}
