import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Note } from 'src/app/models/note.model';
import { Notebook } from 'src/app/models/notebook.model';
import { ApiService } from 'src/app/services/api-service/api.service';
import { DataService } from 'src/app/services/data/data.service';
import { NoteFormComponent } from '../../form/note-form/note-form.component';
import { TranslateService } from '@ngx-translate/core';
import { AppUser } from 'src/app/models/user.model';

@Component({
  selector: 'app-notes-tile-view',
  templateUrl: './notes-tile-view.component.html',
  styleUrls: ['./notes-tile-view.component.scss'],
})
export class NotesTileViewComponent implements OnInit {

  @Input() notebook: Notebook;
  @Input() showAddNote = false;
  @Input() showSearch = false;
  @Input() view = 'TILES';  /** TILES | LIST */
  @Input() exhibitorId: number;

  filter: any = {};

  user: AppUser;

  constructor(
    private router: Router,
    private modalController: ModalController,
    private apiService: ApiService,
    private route: ActivatedRoute,
    private translateService: TranslateService
  ) { }

  ngOnInit() {
    DataService.appUser.subscribe(u => {
      this.user = u;
    });
    this.filter.types = [];
    this.filter.title = '';
    if (!this.notebook) {
      DataService.noteBook.subscribe(data => {
        this.notebook = data;
      });
      this.route.params.subscribe(() => {
        if (this.notebook) {
          this.apiService.getNotebook(this.notebook.id).then(n => {
            DataService.noteBook.next(n);
          });
        }
      });
    }
  }

  isEditable() {
    return this.notebook && this.notebook.isCanEdit(this.user);
  }

  viewIcon() {
    return this.view === 'TILES' ? 'list-outline' : 'grid-outline';
  }

  viewName() {
    return this.view === 'TILES' ?  this.translateService.instant('NOTES_TILE.list_view') : this.translateService.instant('NOTES_TILE.tiles_view') ;
  }

  switchView() {
    if (this.view === 'TILES') {
      this.view = 'LIST';
    } else {
      this.view = 'TILES';
    }
  }

  openNote(note: Note) {
    this.router.navigate(['note', note.id]);
  }

  notesGrid() {
    const grid = [];
    const notes = this.doFilter().reverse();
    for (let i = 0; i < notes.length; i = i + 2) {
      grid.push([
        notes[i], notes[i + 1] ? notes[i + 1] : null
      ]);
    }
    return grid;
  }

  getNotes(){
    return this.doFilter().reverse();
  }

  filterBy(type: string) {
    if (this.filter.types.includes(type)) {
      this.filter.types = this.filter.types.filter(t => t !== type);
    } else {
      this.filter.types.push(type);
    }
  }

  searchByTitle(e) {
    this.filter.title = e.detail.value;
  }

  isTypeSelected(type) {
    return this.filter.types.includes(type);
  }

  newNote() {
    DataService.exhibitor_id = this.exhibitorId;
    this.router.navigate(['tabs/productadd1']);
  }

  // async newNote() {
  //   const modal = await this.modalController.create({
  //     component: NoteFormComponent,
  //     componentProps: {
  //       notebook: this.notebook,
  //       showProductFields: true,
  //       afterSubmit: () => {
  //         this.apiService.getNotebook(this.notebook.id).then(data => {
  //           DataService.noteBook.next(data);
  //         });
  //       }
  //     }
  //   });
  //   await modal.present();
  // }

  private doFilter() {
    return this.notebook.notes.filter(note => {
      let passed = true;
      const filterTypes = this.filter.types;
      if (filterTypes.length > 0 && !filterTypes.includes(note.type)) {
        passed = false;
      }
      if (this.filter.title) {
        const isTitleMatch = note.title.toLowerCase().indexOf(this.filter.title.toLowerCase()) !== -1;
        const isContentMatch = note.content && note.content.toLowerCase().indexOf(this.filter.title.toLowerCase()) !== -1;
        if (!isContentMatch && !isTitleMatch) {
          passed = false;
        }
      }
      if (this.exhibitorId && this.exhibitorId !== note.exhibitor_id) {
        passed = false;
      }
      return passed;
    });
  }

}
