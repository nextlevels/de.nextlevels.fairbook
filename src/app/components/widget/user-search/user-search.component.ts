import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll, ModalController } from '@ionic/angular';
import { AppUser } from 'src/app/models/user.model';
import { ApiService } from 'src/app/services/api-service/api.service';
import { SharedService } from 'src/app/services/shared/shared.service';

@Component({
  selector: 'app-user-search',
  templateUrl: './user-search.component.html',
  styleUrls: ['./user-search.component.scss'],
})
export class UserSearchComponent implements OnInit {

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  @Input() onSelect: Function;
  @Input() selected: AppUser[];

  users: AppUser[] = [];

  searchText: string = '';
  page = 1;
  pageSize = 50;

  checkedMap = {};

  loading = false;

  constructor(
    private apiService: ApiService,
    private modalController: ModalController,
    private sharedService: SharedService
  ) { }

  ngOnInit() {
    this.loadUsers();
    this.selected.forEach(u => {
      this.checkedMap[u.id] = u;
    });
  }

  onClose() {
    this.modalController.dismiss();
  }

  onFilterChange(event) {
    this.searchText = event.detail.value;
    this.onFilter();
  }

  onFilter() {
    this.page = 1;
    this.users = [];
    this.loadUsers();
  }

  noResults() {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return this.searchText && this.users.length === 0 && re.test(String(this.searchText).toLowerCase());
  }

  invite() {
    if (this.selected.filter(u => u.email === this.searchText).length > 0) {
      return; // exclude duplicates
    }
    const ids = [];
    this.selected.forEach(u => {
      ids.push(u.id);
    });
    let minId = Math.min(...ids);
    if (!minId || minId > -1) {
      minId = -1;
    } else {
      minId--;
    }
    const fakeUser = new AppUser({
      id: minId,
      first_name: 'Empfohlener',
      last_name: 'Benutzer',
      user: {
        email: this.searchText
      }
    });
    this.onCheck(fakeUser);
    this.searchText = '';
    this.sharedService.presentToast("Die Einladung wurde der Liste hinzugefügt.");
  }

  getName(user: AppUser) {
    return this.highlightText(user.fullname());
  }

  getEmail(user: AppUser) {
    return this.highlightText(user.email);
  }

  isChecked(user: AppUser) {
    return this.checkedMap[user.id] ? true : false;
  }

  onCheck(user: AppUser, event = null) {
    if (this.isChecked(user)) {
      delete this.checkedMap[user.id];
    } else {
      this.checkedMap[user.id] = user;
    }
  }

  loadMore(event) {
    this.page++;
    this.loadUsers(() => {
      event.target.complete();
    });
  }

  onSubmit() {
    this.onSelect(Object.values(this.checkedMap));
  }

  private highlightText(text: string) {
    const idx = text.toLowerCase().indexOf(this.searchText.toLowerCase());
    if (this.searchText.length > 0 && idx !== -1) {
      return `${text.substring(0, idx)}<b>${text.substring(idx, idx + this.searchText.length)}</b>${text.substring(idx + this.searchText.length)}`;
    } else {
      return text;
    }
  }

  private loadUsers(callback = null) {
    this.loading = true;
    this.apiService.searchUsers({
      searchText: this.searchText,
      page: this.page,
      pageSize: this.pageSize
    }).then(data => {
      data.forEach(u => {
        this.users.push(u);
      });
      this.loading = false;
      if (callback) {
        callback();
      }
    });
  }

}
