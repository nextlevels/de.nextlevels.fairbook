import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-iframe',
  templateUrl: './iframe.page.html',
  styleUrls: ['./iframe.page.scss'],
})
export class IframePage implements OnInit {

  _url : any;
  constructor(  private activatedRoute: ActivatedRoute,
                private sanitizer: DomSanitizer) {
   this._url =  this.sanitizer.bypassSecurityTrustResourceUrl("https://fairbook.de/" + this.activatedRoute.snapshot.paramMap.get('url'));
  }

  ngOnInit() {
  }

}
