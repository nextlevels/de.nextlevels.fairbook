import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MessebucherPageRoutingModule } from './messebucher-routing.module';

import { MessebucherPage } from './messebucher.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    MessebucherPageRoutingModule
  ],
  declarations: [MessebucherPage]
})
export class MessebucherPageModule {}
