import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MessebucherPage } from './messebucher.page';

describe('MessebucherPage', () => {
  let component: MessebucherPage;
  let fixture: ComponentFixture<MessebucherPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessebucherPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MessebucherPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
