import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {DataService} from '../services/data/data.service';
import m_Book from '../models/book.model';
import {FormControl} from '@angular/forms';
import {debounceTime} from 'rxjs/operators';

@Component({
    selector: 'app-messebucher',
    templateUrl: './messebucher.page.html',
    styleUrls: ['./messebucher.page.scss'],
})
export class MessebucherPage implements OnInit {

    _books: m_Book[];
    _filteredBooks: m_Book[];

    searchTerm = '';
    _filterTeam = false;
    _filterOwn = false;

    searchControl: FormControl;
    searching: any = false;

    @ViewChild('content', {static: false}) content;

    constructor(public route: Router,
                public data: DataService) {
        this.searchControl = new FormControl();
    }

    ngOnInit() {
        this.scrollToTopOnInit();

        this.data.books.subscribe(books => {
            this._books = books;
            this._filteredBooks = books;
        });


        this.searchControl.valueChanges
            .pipe(debounceTime(1000))
            .subscribe(search => {
                if (search.length === 0) {
                    this.resetFilter();
                    this.searching = false;
                } else {
                    if (search.length > 2) {
                        this.resetFilter();
                        this._filteredBooks = [...this._books].filter(book => {
                            const searchString = `${book.name}`;
                            return searchString.toLowerCase().indexOf(search.toLowerCase()) > -1;
                        });
                    }
                }
            });
    }

    resetFilter() {
        this._filteredBooks = this._books;
    }

    filterShared() {
        if (!this._filterTeam) {
            this._filteredBooks = this._books.filter(function(book) {
                return book.isShared === true;
            });
            this._filterTeam = true;
        } else {
            this.resetFilter();
            this._filterTeam = false;
        }
    }

    filterOwn() {
        if (!this._filterOwn) {
            this._filteredBooks = this._books.filter(function(book) {
                return book.isShared === false;
            });
            this._filterOwn = true;
        } else {
            this.resetFilter();
            this._filterOwn = false;
        }
    }

    onSearchInput() {
        this.searching = true;
    }

    ionViewWillEnter() {
        this.scrollToTopOnInit();
    }


    goTo3() {
        this.route.navigateByUrl('messebucher1');
    }

    goTo() {
        this.route.navigateByUrl('tabs/notes-mixed');
    }

    getCountShared() {
        return this._books.filter(function(book) {
            return book.isShared === true;
        }).length;
    }

    getCountOwn() {
        return this._books.filter(function(book) {
            return book.isShared === false;
        }).length;
    }


    scrollToTopOnInit() {
        setTimeout(() => {
            if (this.content.scrollToTop) {
                this.content.scrollToTop(400);
            }
        }, 500);
    }


}
