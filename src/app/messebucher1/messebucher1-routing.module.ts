import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Messebucher1Page } from './messebucher1.page';

const routes: Routes = [
  {
    path: '',
    component: Messebucher1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Messebucher1PageRoutingModule {}
