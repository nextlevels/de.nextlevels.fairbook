import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Messebucher1Page } from './messebucher1.page';

describe('Messebucher1Page', () => {
  let component: Messebucher1Page;
  let fixture: ComponentFixture<Messebucher1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Messebucher1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Messebucher1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
