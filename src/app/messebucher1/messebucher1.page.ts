import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-messebucher1',
  templateUrl: './messebucher1.page.html',
  styleUrls: ['./messebucher1.page.scss'],
})
export class Messebucher1Page implements OnInit {

  @ViewChild('content',{static:false}) content;

  constructor(public route: Router) { }

  ngOnInit() {
    this.scrollToTopOnInit();
  }

  ionViewWillEnter(){
    this.scrollToTopOnInit();
  }

  back(){
    this.route.navigateByUrl('messebucher');
  }

  exhibtioner(){
    this.route.navigateByUrl('exhibtioner1');
  }

  exhibtioner2(){
    this.route.navigateByUrl('tabs/notes-mixed');
  }

  scrollToTopOnInit(){
    setTimeout(() => {
      if (this.content.scrollToTop) {
          this.content.scrollToTop(400);
      }
  }, 500);
  }

}
