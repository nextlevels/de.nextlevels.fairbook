export class Attachment {

    id: number;
    file_size: number;
    file_name: string;
    data: string = null;
    path: string;
    thumb: string;
    content_type: string;

    constructor(data) {
        if (data) {
            this.id = data.id;
            this.file_size = data.file_size;
            this.file_name = data.file_name;
            this.data = data.data;
            this.path = data.path;
            this.thumb = data.thumb;
            this.content_type = data.content_type;
        }
    }
}