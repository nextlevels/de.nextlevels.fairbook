export class m_Book {
    id: number;
    name: string;
    isActive: boolean;
    isShared: boolean;
    keywords: string;

    constructor(
        id: number,
        name: string,
        isActive: boolean,
        isShared: boolean,
        keywords: string,
    ) {
        this.id = id;
        this.name = name;
        this.isActive = isActive;
        this.isShared = isShared;
        this.keywords = keywords;
    }
}

export default m_Book;
