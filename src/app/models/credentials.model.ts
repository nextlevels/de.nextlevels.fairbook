export default interface m_Credentials {
    email: string;
    password: string;
}
