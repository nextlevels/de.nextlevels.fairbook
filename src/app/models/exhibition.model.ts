export class Exhibition {
    id: number;
    name: string;
    description: string;
    starts_at: string;
    ends_at: string;
    schedule: string;
    logo: any;
    banner: any;

    constructor(data: any) {
        this.id = data.id;
        this.name = data.name;
        this.description = data.description;
        this.starts_at = data.starts_at;
        this.ends_at = data.ends_at;
        this.schedule = data.schedule;
        this.logo = data.logo;
        this.banner = data.banner;
    }
}

