export class m_Exhibitor {
    id: number;
    name: string;
    visited: boolean;
    bookmarked: boolean;
    logo: any;
    banner: any;
    pivot: any;
    catalogs: any[];
    address: any;

    constructor(
        id: number,
        name: string,
        logo: any,
        pivot: any,
        visited: boolean,
        bookmarked: boolean,
        banner: any,
        catalogs: any[],
        address: any
    ) {
        this.id = id;
        this.name = name;
        this.logo = logo;
        this.pivot = pivot;
        this.visited = visited;
        this.bookmarked = bookmarked;
        this.banner = banner;
        this.catalogs = catalogs;
        this.address = address;
    }
}
export default m_Exhibitor;
