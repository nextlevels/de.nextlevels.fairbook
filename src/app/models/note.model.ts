import * as moment from 'moment';
import { Attachment } from './attachment.model';
moment.locale('de');

export const TYPE_TEXT = {
    label: 'Text',
    value: 'TEXT'
};
export const TYPE_IMAGE = {
    label: 'Bild',
    value: 'IMAGE'
};
export const TYPE_AUDIO = {
    label: 'Audio',
    value: 'AUDIO'
};

export const TYPES = [TYPE_TEXT, TYPE_IMAGE, TYPE_AUDIO];

export class Note {
    id: number;
    title: string;
    content: string;
    type: string;
    file: Attachment;
    created_at: string;
    notebook_id: number;
    product_name: string;
    article_id: string;
    exhibitor_id: number;
    exhibitor: any;

    constructor(data: any) {
        this.id = data.id;
        this.title = data.title;
        this.content = data.content;
        if (data.file) {
            this.file = new Attachment(data.file);
        }
        this.type = data.type;
        this.created_at = moment(data.created_at).format('ll');
        this.notebook_id = data.notebook_id;
        this.product_name = data.product_name;
        this.article_id = data.article_id;
        this.exhibitor_id = data.exhibitor_id;
        this.exhibitor = data.exhibitor;
    }

    public toString() {
        let content = '';
        content += '\n';
        content += this.title + '\n';
        content += '---------------------------\n';
        content += this.content ? this.content : '';
        content += this.file ? this.file.path : '';
        content += '\n';
        content += '---------------------------\n';
        return content;
    }
}

