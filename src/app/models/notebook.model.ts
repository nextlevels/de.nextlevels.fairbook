import { Exhibition } from './exhibition.model';
import { Note } from './note.model';
import * as moment from 'moment';
import { AppUser } from './user.model';
moment.locale('de');

export class Notebook {
    id: number;
    title: string;
    description: string;
    exhibition: Exhibition;
    exhibition_id: number;
    notes: Note[] = [];
    isActive: boolean;
    is_shared: boolean;
    keywords: string;
    created_at: string;
    owner_id: number;
    participants: AppUser[] = [];

    constructor(data: any) {
        this.id = data.id;
        this.title = data.title;
        this.description = data.description;
        this.exhibition_id = data.exhibition_id;
        this.isActive = true;
        this.is_shared = data.is_shared;
        this.created_at = moment(data.created_at).format('ll');
        this.owner_id = data.owner_id;
        if (data.exhibition) {
            this.exhibition = new Exhibition(data.exhibition);
        }
        if (data.notes) {
            data.notes.forEach(item => {
                this.notes.push(new Note(item));
            });
        }
        if (data.participants) {
            data.participants.forEach(p => {
                this.participants.push(new AppUser(p));
            });
        }
    }

    public isCanEdit(user: AppUser) {
        return user && user.id === this.owner_id;
    }
}

