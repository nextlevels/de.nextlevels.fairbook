export class m_Note {
    id: number;
    name: string;
    text: string;
    type: string;
    filePath: string;
    keywords: string;
    exh_id: number;

    constructor(
        id: number,
        name: string,
        text: string,
        type: string,
        filePath: string,
        keywords: string,
        exh_id: number,
    ) {
        this.id = id;
        this.name = name;
        this.text = text;
        this.type = type;
        this.filePath = filePath;
        this.keywords = keywords;
        this.exh_id = exh_id;
    }
}

export default m_Note;
