export class Principal {

    user: User;
    token: string;

    constructor(authData: AuthData) {
        this.user = authData.user;
        this.token = authData.token;
    }
}

export class AppUser {
    id: number;
    first_name: string;
    last_name: string;
    company: string;
    google_user_id: string;
    email: string;

    constructor(data: any) {
        this.id = data.id;
        this.first_name = data.first_name;
        this.last_name = data.last_name;
        this.company = data.company;
        this.google_user_id = data.google_user_id;
        if (data.user) {
            this.email = data.user.email;
        }
    }

    public fullname() {
        return `${this.first_name ? (this.first_name + ' ') : ''}${this.last_name}`;
    }
}

interface AuthData {
    token: string;
    user: User;
}

export interface User {
    id: number;
    name: string;
    email: string;
    permissions: string;
    is_activated: string;
    activated_at: string;
    last_login: string;
    created_at: string;
    updated_at: string;
    username: string;
    surname: string;
    deleted_at: string;
    last_seen: string;
    is_guest: number;
    is_superuser: number;
    created_ip_address: string;
    last_ip_address: string;
    first_name: string;
    last_name: string;
    phone_number: string;
    salutation: string;
    was_ill: boolean;
    is_corona: boolean;
    address_id: string;
    company_id: string;
    address: m_address;
}

interface m_address{
    street: string;
    street_number: string;
    city: string;
    postal_code: string;
}