import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddAudioNotePagePage } from './add-audio-note-page.page';

const routes: Routes = [
  {
    path: '',
    component: AddAudioNotePagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddAudioNotePagePageRoutingModule {}
