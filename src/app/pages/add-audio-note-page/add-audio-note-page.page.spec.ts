import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddAudioNotePagePage } from './add-audio-note-page.page';

describe('AddAudioNotePagePage', () => {
  let component: AddAudioNotePagePage;
  let fixture: ComponentFixture<AddAudioNotePagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAudioNotePagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddAudioNotePagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
