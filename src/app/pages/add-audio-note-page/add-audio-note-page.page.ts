import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Note, TYPE_AUDIO } from 'src/app/models/note.model';
import { ApiService } from 'src/app/services/api-service/api.service';
import { DataService } from 'src/app/services/data/data.service';


@Component({
  selector: 'app-add-audio-note-page',
  templateUrl: './add-audio-note-page.page.html',
  styleUrls: ['./add-audio-note-page.page.scss'],
})
export class AddAudioNotePagePage implements OnInit {
  form: FormGroup;
  @Input() note: Note;
  @Input() afterSubmit: Function;
  @Input() onSubmit: Function;

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private modalController: ModalController,
    private location: Location
  ) { }

  back() {
    if (this.afterSubmit || this.onSubmit) {
      this.modalController.dismiss();
    } else {
      this.location.back();
      this.location.back();
    }
  }

  ngOnInit() {
    this.initForm();
    DataService.noteBook.subscribe(notebook => {
      this.form.patchValue({
        notebook_id: notebook ? notebook.id : null,
        exhibitor_id: DataService.exhibitor_id
      });
    });
    if (this.note) {
      this.form.patchValue({
        id: this.note.id,
        title: this.note.title,
        file: this.note.file,
        exhibitor_id: this.note.exhibitor_id
      });
    }
  }

  submit() {
    const data = this.form.value;
    if (this.onSubmit) {
      this.onSubmit(data);
      this.back();
    } else {
      if (data.id) {
        this.apiService.updateNote(data).then(() => {
          if (this.afterSubmit) {
            this.afterSubmit();
          }
          this.back();
        });
      } else {
        this.apiService.createNote(data).then(() => {
          if (this.afterSubmit) {
            this.afterSubmit();
          }
          this.back();
        });
      }
    }
  }

  private initForm() {
    this.form = this.formBuilder.group({
      id: new FormControl(null, []),
      title: new FormControl(null, [Validators.required, Validators.maxLength(65535)]),
      file: new FormControl(null, [Validators.required]),
      type: new FormControl(TYPE_AUDIO.value, [Validators.required]),
      notebook_id: new FormControl(null, [Validators.required]),
      exhibitor_id: new FormControl(null, [])
    });
  }

}
