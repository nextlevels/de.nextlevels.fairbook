import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddNotePagePage } from './add-note-page.page';

const routes: Routes = [
  {
    path: '',
    component: AddNotePagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddNotePagePageRoutingModule {}
