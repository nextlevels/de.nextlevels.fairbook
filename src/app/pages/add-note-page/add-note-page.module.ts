import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddNotePagePageRoutingModule } from './add-note-page-routing.module';

import { AddNotePagePage } from './add-note-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddNotePagePageRoutingModule
  ],
  declarations: [AddNotePagePage]
})
export class AddNotePagePageModule {}
