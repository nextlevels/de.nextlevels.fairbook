import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddNotePagePage } from './add-note-page.page';

describe('AddNotePagePage', () => {
  let component: AddNotePagePage;
  let fixture: ComponentFixture<AddNotePagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNotePagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddNotePagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
