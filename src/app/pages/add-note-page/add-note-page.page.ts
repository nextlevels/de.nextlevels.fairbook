import { Component, OnInit,ViewChild } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-note-page',
  templateUrl: './add-note-page.page.html',
  styleUrls: ['./add-note-page.page.scss'],
})
export class AddNotePagePage implements OnInit {

  @ViewChild('content',{static:false}) content;

  constructor(private route: Router) { }

  go(){
    this.route.navigate(["tabs/add-audio-note-page"])
  }

  goTextnote(){
    this.route.navigate(['tabs/add-textnote-page'])
  }


return(){
  this.route.navigate(['tabs/add-page'])
}

  ngOnInit() {
    this.scrollToTopOnInit();
  }

  ionViewWillEnter(){
    this.scrollToTopOnInit();
  }

  goPhoto() {};

  scrollToTopOnInit(){
    setTimeout(() => {
      if (this.content.scrollToTop) {
          this.content.scrollToTop(400);
      }
  }, 500);
  }

}
