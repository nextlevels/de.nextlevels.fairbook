import { Component, OnInit,ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-page',
  templateUrl: './add-page.page.html',
  styleUrls: ['./add-page.page.scss'],
})
export class AddPagePage implements OnInit {

  @ViewChild('content',{static:false}) content;

  constructor(private route: Router) { }


  goNote(){
    this.route.navigate(['tabs/add-note-page'])
  }

  goProduct(){
    this.route.navigate(['tabs/productadd'])
  }

  ngOnInit() {
    this.scrollToTopOnInit();
  }

  ionViewWillEnter(){
    this.scrollToTopOnInit();
  }

  back(){
    this.route.navigate(['tabs/katalog']);

  }

  return() {};

  scrollToTopOnInit(){
    setTimeout(() => {
      if (this.content.scrollToTop) {
          this.content.scrollToTop(400);
      }
  }, 500);
  }

}
