import { Component, OnInit,ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.page.html',
  styleUrls: ['./add-product.page.scss'],
})
export class AddProductPage implements OnInit {

  @ViewChild('content',{static:false}) content;

  constructor(private route: Router, public storage:Storage) { }

  return(){
    this.route.navigate(['tabs/add-page'])
  }

  goTextnote(){
    
    this.route.navigate(['tabs/add-textnote-page'])
  }

  goAudionote(){
  
    this.route.navigate(['tabs/add-audio-note-page'])
  }

  ngOnInit() {
    this.scrollToTopOnInit();
  }

  ionViewWillEnter(){
    this.scrollToTopOnInit();
  }

  scrollToTopOnInit(){
    setTimeout(() => {
      if (this.content.scrollToTop) {
          this.content.scrollToTop(400);
      }
  }, 500);
  }

}
