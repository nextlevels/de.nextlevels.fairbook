import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {ModalController, NavController} from '@ionic/angular';
import { Note, TYPE_TEXT } from 'src/app/models/note.model';
import { ApiService } from 'src/app/services/api-service/api.service';
import { DataService } from 'src/app/services/data/data.service';


@Component({
  selector: 'app-add-textnote-page',
  templateUrl: './add-textnote-page.page.html',
  styleUrls: ['./add-textnote-page.page.scss'],
})
export class AddTextnotePagePage implements OnInit {

  form: FormGroup;
  @Input() note: Note;
  @Input() afterSubmit: Function;
  @Input() onSubmit: Function;

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private location: Location,
     private modalController: ModalController
  ) { }

  back() {
    if (this.afterSubmit || this.onSubmit) {
      this.modalController.dismiss();
    } else {
      this.location.back();
      this.location.back();
    }
  }

  ngOnInit() {
    this.initForm();
    DataService.noteBook.subscribe(notebook => {
      this.form.patchValue({
        notebook_id: notebook ? notebook.id : null,
        exhibitor_id: DataService.exhibitor_id
      });
    });
    if (this.note) {
      this.form.patchValue({
        id: this.note.id,
        title: this.note.title,
        content: this.note.content,
        exhibitor_id: this.note.exhibitor_id,
      });
    }
  }

  submit() {
    const data = this.form.value;
    if (this.onSubmit) {
      this.onSubmit(data);
      this.back();
    } else {
      if (data.id) {
        this.apiService.updateNote(data).then(() => {
          if (this.afterSubmit) {
            this.afterSubmit();
          }
          this.back();
        });
      } else {
        this.apiService.createNote(data).then(() => {
          if (this.afterSubmit) {
            this.afterSubmit();
          }
          this.back();
        });
      }
    }
  }

  private initForm() {
    this.form = this.formBuilder.group({
      id: new FormControl(null, []),
      title: new FormControl(null, [Validators.required, Validators.maxLength(65535)]),
      content: new FormControl(null, [Validators.required, Validators.maxLength(65535)]),
      type: new FormControl(TYPE_TEXT.value, [Validators.required]),
      notebook_id: new FormControl(null, [Validators.required]),
      exhibitor_id: new FormControl(null, [])
    });
  }

}
