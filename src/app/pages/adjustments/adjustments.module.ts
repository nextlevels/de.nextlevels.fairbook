import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdjustmentsPageRoutingModule } from './adjustments-routing.module';

import { AdjustmentsPage } from './adjustments.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdjustmentsPageRoutingModule
  ],
  declarations: [AdjustmentsPage]
})
export class AdjustmentsPageModule {}
