import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AdjustmentsPage } from './adjustments.page';

describe('AdjustmentsPage', () => {
  let component: AdjustmentsPage;
  let fixture: ComponentFixture<AdjustmentsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdjustmentsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AdjustmentsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
