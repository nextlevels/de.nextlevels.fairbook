import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AnfragenSeitePageRoutingModule } from './anfragen-seite-routing.module';

import { AnfragenSeitePage } from './anfragen-seite.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AnfragenSeitePageRoutingModule
  ],
  declarations: [AnfragenSeitePage]
})
export class AnfragenSeitePageModule {}
