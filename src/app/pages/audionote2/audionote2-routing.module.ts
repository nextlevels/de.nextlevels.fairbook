import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Audionote2Page } from './audionote2.page';

const routes: Routes = [
  {
    path: '',
    component: Audionote2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Audionote2PageRoutingModule {}
