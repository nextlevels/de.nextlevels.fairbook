import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Audionote2PageRoutingModule } from './audionote2-routing.module';

import { Audionote2Page } from './audionote2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Audionote2PageRoutingModule
  ],
  declarations: [Audionote2Page]
})
export class Audionote2PageModule {}
