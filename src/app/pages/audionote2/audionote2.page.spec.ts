import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Audionote2Page } from './audionote2.page';

describe('Audionote2Page', () => {
  let component: Audionote2Page;
  let fixture: ComponentFixture<Audionote2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Audionote2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Audionote2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
