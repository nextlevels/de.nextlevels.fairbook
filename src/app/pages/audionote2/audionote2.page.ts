import { Component, OnInit,ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-audionote2',
  templateUrl: './audionote2.page.html',
  styleUrls: ['./audionote2.page.scss'],
})
export class Audionote2Page implements OnInit {

  @ViewChild('content',{static:false}) content;

  constructor(public router: Router) { }

  ngOnInit() {
    this.scrollToTopOnInit();
  }

  ionViewWillEnter(){
    this.scrollToTopOnInit();
  }

  return(){
    this.router.navigateByUrl('tabs/productadd1');
  }

  go() {};

  scrollToTopOnInit(){
    setTimeout(() => {
      if (this.content.scrollToTop) {
          this.content.scrollToTop(400);
      }
  }, 500);
  }

}
