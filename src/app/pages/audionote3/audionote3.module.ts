import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Audionote3PageRoutingModule } from './audionote3-routing.module';

import { Audionote3Page } from './audionote3.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Audionote3PageRoutingModule
  ],
  declarations: [Audionote3Page]
})
export class Audionote3PageModule {}
