import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Audionote3Page } from './audionote3.page';

describe('Audionote3Page', () => {
  let component: Audionote3Page;
  let fixture: ComponentFixture<Audionote3Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Audionote3Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Audionote3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
