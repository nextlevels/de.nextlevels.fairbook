import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { ApiService } from 'src/app/services/api-service/api.service';
import { DataService } from 'src/app/services/data/data.service';
import { SharedService } from 'src/app/services/shared/shared.service';
import { SignInWithApple } from '@ionic-native/sign-in-with-apple/ngx';
import { Md5 } from 'ts-md5/dist/md5';
import {Platform} from '@ionic/angular';
import { StorageService } from 'src/app/services/storage/storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  passwordVisibility = false;
  form: any;

  constructor(
    private route: Router,
    private formBuilder: FormBuilder,
    private googlePlus: GooglePlus,
    private signInWithApple: SignInWithApple,
    private apiService: ApiService,
    private sharedService: SharedService,
    private platform: Platform,
    private storageService: StorageService
  ) {}

  ngOnInit() {
    this.storageService.get('autoLogin').then(autoLogin => {
      this.form = this.formBuilder.group({
        email: new FormControl(null, [Validators.required]),
        password: new FormControl(null, [Validators.required]),
        autoLogin: new FormControl(autoLogin === 'true')
      });
    });
  }

  ionViewDidEnter() {
    const subscription = DataService.principal.subscribe(principal => {
      if (principal && principal.user) {
        setTimeout(() => {
          subscription.unsubscribe();
        }, 10);
        console.log('User logged in - redirect to notebooks...');
        this.route.navigate(['notebooks']);
      }
    });
  }

  login() {
    const formData = this.form.value;
    const autoLogin = this.form.value.autoLogin;
    this.storageService.set('autoLogin', autoLogin).then(() => {
      this.apiService.loginUser(formData.email, formData.password).then(data => {
        DataService.principal.next(data);
        this.route.navigate(['notebooks']);
      });
    });
  }

  switchVisibility() {
    this.passwordVisibility = !this.passwordVisibility;
  }

  newRegistration() {
    this.route.navigate(["signup"])
  }

  forgotPassword() {
    this.route.navigateByUrl('passwort-vergessen');
  }

  loginViaGoogle() {
    console.log('Login via Google');
    this.googlePlus.login({}).then(res => {
      //console.log(JSON.stringify(res));
      res.password = res.userId;
      res.password_confirmation = res.userId;
      this.apiService.loginViaGoogle(res).then(() => {
        this.apiService.loginUser(res.email, res.userId).then(data => {
          DataService.principal.next(data);
          this.route.navigate(['']);
        });
      });
    }).catch(err => {
      console.log('Google sign in error: ' + JSON.stringify(err));
      this.sharedService.simpleAlert('Fehler!', 'Anmeldung bei der Anwendung nicht möglich.');
    });
  }

  isGoogle(){
   return this.platform.is('android');
  }

  loginViaApple() {
    console.log('Login via Apple');
    this.signInWithApple.signin({ requestedScopes: [0, 1] }).then(data => {
      const formData: any = {
        email: `${data.user}@apple.de`,
        password: Md5.hashStr(`Fairbook@${data.user}@NextLevels`),
        autoLogin: true
      };

      this.apiService.loginUser(formData.email, formData.password).then(res => {
        DataService.principal.next(res);
        this.route.navigate(['notebooks']);
      });

    }).catch(err => {
      console.log('Apple sign in error: ' + JSON.stringify(err));
      this.sharedService.simpleAlert('Fehler!', 'Anmeldung bei der Anwendung nicht möglich.');
    });
  }
}
