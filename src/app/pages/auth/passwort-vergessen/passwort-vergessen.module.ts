import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PasswortVergessenPageRoutingModule } from './passwort-vergessen-routing.module';
import { PasswortVergessenPage } from './passwort-vergessen.page';
import {ComponentsModule} from '../../../components/components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PasswortVergessenPageRoutingModule,
    ComponentsModule,
    ReactiveFormsModule,
    TranslateModule
  ],
  declarations: [PasswortVergessenPage]
})
export class PasswortVergessenPageModule {}
