import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {ApiService} from '../../../services/api-service/api.service';
import {stringsMatchValidator} from '../../../components/validators/strings-match-validator';
import { Location } from '@angular/common';
import { SharedService } from 'src/app/services/shared/shared.service';

@Component({
  selector: 'app-passwort-vergessen',
  templateUrl: './passwort-vergessen.page.html',
  styleUrls: ['./passwort-vergessen.page.scss'],
})
export class PasswortVergessenPage implements OnInit {

  form: FormGroup;
  passwordVisibility: boolean = false;
  passwordVisibility2: boolean = false;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private location: Location,
    private sharedService: SharedService
  ) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      full: new FormControl(false, []),
      email: new FormControl(null, [Validators.required, Validators.email]),
      code: new FormControl(null, []),
      password: new FormControl(null, []),
      password_confirmation: new FormControl(null, [])
    }, {
      validators: [stringsMatchValidator('password', 'password_confirmation')],
      asyncValidators: [] 
    });
    this.form.get('full').valueChanges.subscribe(flag => {
      this.form.controls.code.setValidators(flag ? [Validators.required] : []);
      this.form.controls.code.updateValueAndValidity();
      this.form.controls.password.setValidators(flag ? [Validators.required] : []);
      this.form.controls.password.updateValueAndValidity();
      this.form.controls.password_confirmation.setValidators(flag ? [Validators.required] : []);
      this.form.controls.password_confirmation.updateValueAndValidity();
    });
  }

  isVisible() {
    return this.form.get('full').value;
  }

  sendActivationCode() {
    this.apiService.forgotPassword(this.form.value.email).then(() => {
      this.form.get('full').setValue(true);
      this.sharedService.presentToast('Bitte prüfe deine Emails.', 'success');
    });
  }

  changePassword() {
    if (this.form.valid) {
      this.apiService.changePassword(this.form.value).then(() => {
        this.router.navigate(['login']);
      });
    } else {
      this.sharedService.presentToast('Bitte füll alle Daten aus. Prüfe deine Mails.', 'warning');
    }
    
  }

  newRegistration() {
    this.router.navigate(["signup"])
  }

  switchVisibility() {
    this.passwordVisibility = !this.passwordVisibility;
  }

  switchVisibility2() {
    this.passwordVisibility2 = !this.passwordVisibility2;
  }

  goBack() {
    this.location.back();
  }

}
