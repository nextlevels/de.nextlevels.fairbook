import { Component, OnInit,ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { stringsMatchValidator } from 'src/app/components/validators/strings-match-validator';
import { ApiService } from 'src/app/services/api-service/api.service';
import { DataService } from 'src/app/services/data/data.service';
import {Location} from '@angular/common';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.page.html',
  styleUrls: ['./profil.page.scss'],
})
export class ProfilPage implements OnInit {

  form: FormGroup;

  constructor(
    private route: Router,
    private formBuilder: FormBuilder,
    private dataService: DataService,
    private apiService: ApiService,
    private location: Location,
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.initForm();
    DataService.appUser.subscribe(appUser => {
      if (appUser) {
        this.form.patchValue({
          company: appUser.company,
          first_name: appUser.first_name,
          last_name: appUser.last_name
        });
      }
    });
    DataService.principal.subscribe(prinicpal => {
      if (prinicpal && prinicpal.user) {
        this.form.patchValue({
          email: prinicpal.user.email
        });
      }
    });
  }

  color(control: string) {
    return this.form.get(control).touched ? (this.form.get(control).errors ? 'danger' : 'success') : '';
  }

  saveProfile() {
    this.apiService.updateUserProfile(this.form.value).then(appUser => {
      DataService.appUser.next(appUser);
      this.modalController.dismiss();
    });
  }

  logout() {
    this.modalController.dismiss();
    setTimeout(() => {
        this.modalController.dismiss();
    }, 1000);
    this.dataService.logout();
  }

  goBack() {
    this.modalController.dismiss();
  }

  private initForm() {
    this.form = this.formBuilder.group({
      company: new FormControl(null, []),
      first_name: new FormControl(null, [Validators.required]),
      last_name: new FormControl(null, [Validators.required]),
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, []),
      password_confirmation: new FormControl(null, []),
      current_password: new FormControl(null, [])
    }, {
        validators: [stringsMatchValidator('password', 'password_confirmation')],
        asyncValidators: [] 
    });
  }

}
