import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { stringsMatchValidator } from 'src/app/components/validators/strings-match-validator';
import { ApiService } from 'src/app/services/api-service/api.service';
import {SharedService} from '../../../services/shared/shared.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  form: FormGroup;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private shared: SharedService,
  ) { }

  ngOnInit() {
    this.initForm();
  }

  color(control: string) {
    return this.form.get(control).touched ? (this.form.get(control).errors ? 'danger' : 'success') : '';
  }

  private initForm() {
    this.form = this.formBuilder.group({
      company: new FormControl(null, []),
      first_name: new FormControl(null, [Validators.required]),
      last_name: new FormControl(null, [Validators.required]),
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required]),
      password_confirmation: new FormControl(null, [Validators.required])
    }, {
        validators: [stringsMatchValidator('password', 'password_confirmation')],
        asyncValidators: []
    });
  }

  registerNewUser() {
    this.apiService.registerNewUser(this.form.value).then(() => {
      // tslint:disable-next-line:max-line-length
      this.shared.simpleAlert('Erfolgreich', 'Bitte prüfe deine Emails und bestätige deine E-Mail Adresse um dich anzumelden.').then(data => {
        if (data){
          this.router.navigateByUrl('login');        }
      });
    }, error => {
      this.shared.errorAlert('Hoppla!', error);
    });
  }

goBack(){
    this.router.navigate(['login']);
  }

}
