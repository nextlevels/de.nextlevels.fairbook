import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BlKatalogPageRoutingModule } from './bl-katalog-routing.module';

import { BlKatalogPage } from './bl-katalog.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BlKatalogPageRoutingModule
  ],
  declarations: [BlKatalogPage]
})
export class BlKatalogPageModule {}
