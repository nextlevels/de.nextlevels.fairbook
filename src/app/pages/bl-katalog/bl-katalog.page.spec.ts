import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BlKatalogPage } from './bl-katalog.page';

describe('BlKatalogPage', () => {
  let component: BlKatalogPage;
  let fixture: ComponentFixture<BlKatalogPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlKatalogPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BlKatalogPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
