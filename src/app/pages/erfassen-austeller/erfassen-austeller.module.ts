import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ErfassenAustellerPageRoutingModule } from './erfassen-austeller-routing.module';

import { ErfassenAustellerPage } from './erfassen-austeller.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ErfassenAustellerPageRoutingModule
  ],
  declarations: [ErfassenAustellerPage]
})
export class ErfassenAustellerPageModule {}
