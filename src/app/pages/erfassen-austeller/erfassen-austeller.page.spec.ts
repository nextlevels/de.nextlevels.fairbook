import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ErfassenAustellerPage } from './erfassen-austeller.page';

describe('ErfassenAustellerPage', () => {
  let component: ErfassenAustellerPage;
  let fixture: ComponentFixture<ErfassenAustellerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErfassenAustellerPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ErfassenAustellerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
