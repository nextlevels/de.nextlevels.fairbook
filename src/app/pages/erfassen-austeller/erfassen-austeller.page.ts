import { Component, OnInit,ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-erfassen-austeller',
  templateUrl: './erfassen-austeller.page.html',
  styleUrls: ['./erfassen-austeller.page.scss'],
})
export class ErfassenAustellerPage implements OnInit {

  @ViewChild('content',{static:false}) content;

  constructor(public router: Router) { }


  ngOnInit() {
    this.scrollToTopOnInit();
  }

  ionViewWillEnter(){
    this.scrollToTopOnInit();
  }

  goToKatalog(){
    this.router.navigate(['tabs/katalog']);
  }
  back(){
    this.router.navigateByUrl('tabs/erfassen1');
  }

  scrollToTopOnInit(){
    setTimeout(() => {
      if (this.content.scrollToTop) {
          this.content.scrollToTop(400);
      }
  }, 500);
  }

}
