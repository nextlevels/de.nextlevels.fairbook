import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Erfassen1Page } from './erfassen1.page';

describe('Erfassen1Page', () => {
  let component: Erfassen1Page;
  let fixture: ComponentFixture<Erfassen1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Erfassen1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Erfassen1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
