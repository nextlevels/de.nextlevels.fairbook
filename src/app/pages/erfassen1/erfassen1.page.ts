import { Component, OnInit,ViewChild } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-erfassen1',
  templateUrl: './erfassen1.page.html',
  styleUrls: ['./erfassen1.page.scss'],
})
export class Erfassen1Page implements OnInit {

  @ViewChild('content',{static:false}) content;

  constructor(private route: Router) { }


  goAussteller(){
    this.route.navigate(['tabs/erfassen-austeller']);
  }

  goAddNote(){
    this.route.navigate(['add-note-page']);
  }

  ngOnInit() {
    this.scrollToTopOnInit();
  }

  ionViewWillEnter(){
    this.scrollToTopOnInit();
  }

  goToerfassen2(){
    this.route.navigate(['tabs/productadd1']);
  }

  back(){
    this.route.navigateByUrl('tabs/notes-mixed');
  }

  scrollToTopOnInit(){
    setTimeout(() => {
      if (this.content.scrollToTop) {
          this.content.scrollToTop(400);
      }
  }, 500);
  }

}
