import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Erfassen2PageRoutingModule } from './erfassen2-routing.module';

import { Erfassen2Page } from './erfassen2.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Erfassen2PageRoutingModule,
    ComponentsModule,
    TranslateModule
  ],
  declarations: [Erfassen2Page]
})
export class Erfassen2PageModule {}
