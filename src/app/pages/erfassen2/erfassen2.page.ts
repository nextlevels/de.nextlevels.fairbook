import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api-service/api.service';
import { FileService } from 'src/app/services/file-service/file.service';
import {DataService} from '../../services/data/data.service';

@Component({
  selector: 'app-erfassen2',
  templateUrl: './erfassen2.page.html',
  styleUrls: ['./erfassen2.page.scss'],
})
export class Erfassen2Page implements OnInit {

  @ViewChild('pwaphoto') pwaphoto: ElementRef;

  imgURI: string = null;
  exhibition_id: number;

  constructor(
    private camera: Camera,
    private router: Router,
    private alertController: AlertController,
    private apiService: ApiService,
    private fileService: FileService
  ) { }

  ngOnInit() {
    DataService.noteBook.subscribe(notebook => {
      if (notebook && notebook.exhibition) {
        this.exhibition_id = notebook.exhibition.id;
      }
    });
  }

  back(){
    this.router.navigateByUrl('tabs/notes-mixed');
  }

  openCamera(){

    // if (this.pwaphoto == null) {
    //   return;
    // }

    // this.pwaphoto.nativeElement.click();
    console.log('Open camera');
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };
    this.camera.getPicture(options).then((imageData) => {
      this.apiService.scanBusinessCard(imageData, this.exhibition_id).then(exhibitor => {
        if (exhibitor) {
          this.router.navigate(['/tabs/exhibitors-detail', exhibitor.id]);
        }
      });
    }, (err) => {
      console.log('Camera error: ' + JSON.stringify(err));
    });

  }

  openGallery() {
    this.fileService.selectFileFromDevice().then(data => {
      this.apiService.scanBusinessCard(data.data, this.exhibition_id).then(exhibitor => {
        if (exhibitor) {
          this.router.navigate(['/tabs/exhibitors-detail', exhibitor.id]);
        }
      });
    });

  }

  uploadPWA() {

    if (this.pwaphoto == null) {
      return;
    }

    const fileList: FileList = this.pwaphoto.nativeElement.files;

    if (fileList && fileList.length > 0) {
      // this.router.navigateByUrl('tabs/katalog3/1');
      this.firstFileToBase64(fileList[0]).then((result: string) => {
        this.imgURI = result;
        this.apiService.scanBusinessCard(result.split(',')[1], this.exhibition_id).then(exhibitor => {
          if (exhibitor) {
            this.router.navigate(['/tabs/exhibitors-detail', exhibitor.id]);
          }
        });
      }, (err: any) => {
        // Ignore error, do nothing
        this.imgURI = null;
      });
    }
  }

  private firstFileToBase64(fileImage: File): Promise<{}> {
    return new Promise((resolve, reject) => {
      const fileReader: FileReader = new FileReader();
      if (fileReader && fileImage != null) {
        fileReader.readAsDataURL(fileImage);
        fileReader.onload = () => {
          resolve(fileReader.result);
        };

        fileReader.onerror = (error) => {
          reject(error);
        };
      } else {
        reject(new Error('No file found'));
      }
    });
  }

  async searchExhibitorByStendNumber() {
    const alert = await this.alertController.create({
      header: 'Aussteller Eingeben',
      inputs: [
        {
          name: 'standnummer',
          type: 'text',
          placeholder: 'Standnummer'
        }
      ],
      buttons: [
        {
          text: 'Abbrechen',
          role: 'cancel'
        }, {
          text: 'Ok',
          handler: (res) => {
            this.apiService.searchByStandNumber(res.standnummer).then(exhibitor => {
              if (exhibitor) {
                this.router.navigate(['/tabs/exhibitors-detail', exhibitor.id]);
              }
            });
          }
        }
      ]
    });

    await alert.present();
  }

}
