import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExhibitionInfoPage } from './exhibition-info.page';

const routes: Routes = [
  {
    path: '',
    component: ExhibitionInfoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExhibitionInfoPageRoutingModule {}
