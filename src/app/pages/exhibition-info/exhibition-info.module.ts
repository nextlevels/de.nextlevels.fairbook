import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExhibitionInfoPageRoutingModule } from './exhibition-info-routing.module';

import { ExhibitionInfoPage } from './exhibition-info.page';
import {ComponentsModule} from '../../components/components.module';
import {TranslateModule} from "@ngx-translate/core";
import {PipeModule} from '../../pipe.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ComponentsModule,
        ExhibitionInfoPageRoutingModule,
        TranslateModule,
        PipeModule
    ],
  declarations: [ExhibitionInfoPage]
})
export class ExhibitionInfoPageModule {}
