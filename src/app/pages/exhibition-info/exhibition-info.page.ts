import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {DataService} from '../../services/data/data.service';
import {ApiService} from '../../services/api-service/api.service';
import {Exhibition} from '../../models/exhibition.model';
import m_Exhibitor from '../../models/exhibitors.model';

@Component({
    selector: 'app-exhibition-info',
    templateUrl: './exhibition-info.page.html',
    styleUrls: ['./exhibition-info.page.scss'],
})
export class ExhibitionInfoPage implements OnInit {

    exhibition: Exhibition;
    exhibitors: m_Exhibitor[];

    slideOpts = {
        initialSlide: 0,
        slidesPerView: 2.7,
        spaceBetween: 10
    };

    constructor(public router: Router,
                public data: DataService,
                private apiService: ApiService) {
    }

    @ViewChild('content', {static: false}) content;

    ngOnInit() {
        DataService.noteBook.subscribe(notebook => {
            if (notebook) {
                this.exhibition = notebook.exhibition;
                if (notebook && notebook.exhibition) {
                    this.apiService.getAllExhibitiors(notebook.exhibition.id).then(data => {
                        this.exhibitors = data;
                        console.log(this.exhibitors);
                    });
                }
            }
        });
        this.scrollToTopOnInit();
    }

    ionViewWillEnter() {
        this.scrollToTopOnInit();
    }

    austeller() {
        this.router.navigate(['tabs/exhibitors-list']);
    }

    goTo() {
        this.router.navigateByUrl('tabs/katalog3');
    }

    goToPage() {
        this.router.navigateByUrl('exhibtioner1');
    }

    scrollToTopOnInit() {
        setTimeout(() => {
            if (this.content.scrollToTop) {
                this.content.scrollToTop(400);
            }
        }, 500);
    }

}
