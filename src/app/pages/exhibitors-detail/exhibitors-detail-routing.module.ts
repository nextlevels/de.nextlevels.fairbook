import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExhibitorsDetailPage } from './exhibitors-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ExhibitorsDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExhibitorsDetailPageRoutingModule {}
