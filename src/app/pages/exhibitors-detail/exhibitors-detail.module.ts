import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExhibitorsDetailPageRoutingModule } from './exhibitors-detail-routing.module';

import { ExhibitorsDetailPage } from './exhibitors-detail.page';
import {ComponentsModule} from '../../components/components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    ExhibitorsDetailPageRoutingModule,
    TranslateModule
  ],
  declarations: [ExhibitorsDetailPage]
})
export class ExhibitorsDetailPageModule {}
