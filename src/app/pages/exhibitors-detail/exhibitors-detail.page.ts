import { Component, OnInit, ViewChild } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { map } from 'rxjs/operators';
import m_Note from '../../models/notes.model';
import { DataService } from '../../services/data/data.service';
import { ApiService } from '../../services/api-service/api.service';
import { SocialSharingPage } from '../../social-sharing/social-sharing.page';
import m_Exhibitor from 'src/app/models/exhibitors.model';


@Component({
  selector: 'app-exhibitors-detail',
  templateUrl: './exhibitors-detail.page.html',
  styleUrls: ['./exhibitors-detail.page.scss'],
})
export class ExhibitorsDetailPage implements OnInit {


  constructor(
    private socialSharing: SocialSharing,
    public router: Router,
    public data: DataService,
    private activatedRoute: ActivatedRoute,
    public modalController: ModalController,
    private apiService: ApiService
  ) {

    const id = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    this._exhibitorId = id;
    this.apiService.getExhibitorById(id).then(exhibitor => {
        this._exhibitor = exhibitor;
        // console.log(this._exhibitor);
    });

    this.apiService.addExhibitor(id).then(exhibitor => {
    });


  }

  _listView = false;
  _filterCam = false;
  _filterText = false;
  _filterVoice = false;
  _exhibitor: m_Exhibitor;
  _exhibitorId: number;

  isNotizen = true;
  isCamera = true;
  isArticle = true;
  isAudio = true;

  _notes: m_Note[];
  _filteredNotes: m_Note[];

  slideOpts = {
    initialSlide: 0,
    slidesPerView: 2.5
  };

  slideOpts2 = {
    initialSlide: 0,
    slidesPerView: 1
  };

  showCamera() { }  showNotizen() { }  showArticle() { }  showAudio() { }
  ngOnInit() {
    this.data.activeExhibitor = this._exhibitor;
  }


  async presentModal() {
    const modal = await this.modalController.create({
      component: SocialSharingPage,
      cssClass: 'socialSharingModal'
    });
    return await modal.present();
  }

  anfragen() {
        this.socialSharing.shareViaEmail('Hallo', 'Request from Fairbook ', [this._exhibitor.address.email]).then(() => {
          // Success!
        }).catch(() => {
          // Error!
        });
  }

  addPinnwand() {
    this.router.navigateByUrl('tabs/productadd1');
  }

  LISTENANSICHT() {
    this._listView = !this._listView;
  }

  isBookmarked(){
    false;
  }

  bookmark(){
    this.apiService.bookMarkExhibitor(this._exhibitor.id).then(exhibitor => {
    });
  }

  filterBy(type) {
    if ((type === 'image' && this._filterCam === true) || (type === 'text' && this._filterText === true) || (type === 'voice' && this._filterVoice === true)) {
      this._filteredNotes = this._notes;
      this.setActive('');
    } else {
      this._filteredNotes = this._notes.filter(function(note) {
        return note.type === type;
      });
      this.setActive(type);
    }
  }

  openCatalog(catalog){
    window.open(catalog.pdf.path, '_system', 'location=yes');
  }

  setActive(type) {
    this._filterCam = false;
    this._filterText = false;
    this._filterVoice = false;
    switch (type) {
      case 'image':
        this._filterCam = true;
        break;
      case 'text':
        this._filterText = true;
        break;
      case 'voice':
        this._filterVoice = true;
        break;
    }
  }

  share() {
    DataService.noteBook.subscribe(notebook => {
      let content = '===========================\n';
      content += this._exhibitor.name + '\n';
      content += '===========================\n\n';
      notebook.notes.filter(note => {
        return note.exhibitor_id === this._exhibitorId;
      }).forEach(note => {
        content += note.toString();
      });
      this.socialSharing.shareWithOptions({
        message: content,
        subject: this._exhibitor.name
      });
    });
  }

}
