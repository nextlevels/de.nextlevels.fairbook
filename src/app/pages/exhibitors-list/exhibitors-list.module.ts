import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExhibitorsListPageRoutingModule } from './exhibitors-list-routing.module';

import { ExhibitorsListPage } from './exhibitors-list.page';
import {ComponentsModule} from '../../components/components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    ExhibitorsListPageRoutingModule,
    TranslateModule
  ],
  declarations: [ExhibitorsListPage]
})
export class ExhibitorsListPageModule {}
