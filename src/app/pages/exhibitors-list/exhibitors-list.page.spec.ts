import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ExhibitorsListPage } from './exhibitors-list.page';

describe('ExhibitorsListPage', () => {
  let component: ExhibitorsListPage;
  let fixture: ComponentFixture<ExhibitorsListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExhibitorsListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ExhibitorsListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
