import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import m_Exhibitor from '../../models/exhibitors.model';
import { DataService } from '../../services/data/data.service';
import { ApiService } from '../../services/api-service/api.service';

@Component({
  selector: 'app-exhibitors-list',
  templateUrl: './exhibitors-list.page.html',
  styleUrls: ['./exhibitors-list.page.scss'],
})
export class ExhibitorsListPage implements OnInit {

  @ViewChild('content', { static: false }) content;

  _exhibitors: m_Exhibitor[];
  _exhibitorsUser: m_Exhibitor[];
  _filteredExhibitors: m_Exhibitor[];

  searchText = '';

  constructor(public router: Router,
    public data: DataService,
    private apiService: ApiService) { }

  ngOnInit() {
    this.scrollToTopOnInit();

    DataService.noteBook.subscribe(notebook => {
      console.log(notebook);
      if (notebook && notebook.exhibition) {
        this.apiService.getAllExhibitiors(notebook.exhibition.id).then(data => {
          this._exhibitors = data;
          this._filteredExhibitors = data;
        });
        this.apiService.getAllExhibitiorsUser(notebook.exhibition.id).then(data => {
          this._exhibitorsUser = data;
        });
      }
    });
  }

  ionViewWillEnter() {
    this.scrollToTopOnInit();
  }

  showAll() {
    this._filteredExhibitors = this._exhibitors;
  }

  showVisited() {
    this._filteredExhibitors = this._exhibitorsUser;
  }

  showMarked() {
    this._filteredExhibitors = this._exhibitorsUser.filter(function (exhibitor) {
      return exhibitor.pivot.is_bookmarked === 1;
    });
  }

  searchByText(e) {
    this.searchText = e.detail.value;
    this.doFilter();
  }

  private doFilter() {
    this._filteredExhibitors = this._exhibitors.filter(ex => {
      let passed = true;
      if (this.searchText && ex.name.toLowerCase().indexOf(this.searchText.toLowerCase()) === -1) {
        passed = false;
      }
      return passed;
    });
  }


  scrollToTopOnInit() {
    setTimeout(() => {
      if (this.content.scrollToTop) {
        this.content.scrollToTop(400);
      }
    }, 500);
  }

}
