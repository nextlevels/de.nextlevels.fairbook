import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Exhibtioner1Page } from './exhibtioner1.page';

describe('Exhibtioner1Page', () => {
  let component: Exhibtioner1Page;
  let fixture: ComponentFixture<Exhibtioner1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Exhibtioner1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Exhibtioner1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
