import { Component, OnInit,ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-exhibtioner1',
  templateUrl: './exhibtioner1.page.html',
  styleUrls: ['./exhibtioner1.page.scss'],
 
})
export class Exhibtioner1Page implements OnInit {

  @ViewChild('content',{static:false}) content;

  previousIndex=0;

  array=[{"title":"Aktuell","text1":"PSI Messe","text2":"Leipziger Buchmesse",
  "text3":"Messe 3","isSelected":true,"itemSelected":"text1"},
  {"title":"Zukunft","text1":"Frankfurter Buchmesse","text2":"Leipziger Buchmesse",
  "text3":"Messe 3","isSelected":false,"itemSelected":"text1"},
  {"title":"Vergangenheit","text1":"Frankfurter Buchmesse","text2":"Leipziger Buchmesse",
  "text3":"Messe 3","isSelected":false,"itemSelected":"text1"}];

  selected:boolean=true;
  selected1:boolean=true;
  selected2:boolean=true;
  visibility: string = 'hidden';

  constructor(private route: Router,public navCtrl:NavController) { }


  ngOnInit() {
    this.scrollToTopOnInit();
  }

  subItemSelect(index,subitem){
    console.log(index);
    console.log(subitem);
this.array[index].itemSelected=subitem;
console.log(this.array);
  }

  select(i){
    this.array[this.previousIndex].isSelected=false;
    this.array[i].isSelected=true;
    this.previousIndex=i;
  }

  goNext(){
    this.route.navigateByUrl('tabs/notes-mixed');
  }

  ionViewWillEnter(){
    this.scrollToTopOnInit();
  }


  nextPage(){
    this.route.navigateByUrl('tabs/notes-mixed');
  }

  select1(){
    this.selected=true;
    this.selected1=false;
    this.selected2=false;
  }
  select2(){
    this.selected1=true;
    this.selected=false;
    this.selected2=false;
  }
  select3(){
    this.selected2=true;
    this.selected=false;
    this.selected1=false;
  }

  scrollToTopOnInit(){
    setTimeout(() => {
      if (this.content.scrollToTop) {
          this.content.scrollToTop(400);
      }
  }, 500);
  }

}

