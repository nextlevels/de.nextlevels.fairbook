import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotePagePage } from './note-page.page';

const routes: Routes = [
  {
    path: '',
    component: NotePagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotePagePageRoutingModule {}
