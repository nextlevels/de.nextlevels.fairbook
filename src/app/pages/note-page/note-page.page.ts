import { Component, OnInit,ViewChild } from '@angular/core';

@Component({
  selector: 'app-note-page',
  templateUrl: './note-page.page.html',
  styleUrls: ['./note-page.page.scss'],
})
export class NotePagePage implements OnInit {

  @ViewChild('content',{static:false}) content;

  constructor() { }

  ngOnInit() {
    this.scrollToTopOnInit();
  }

  ionViewWillEnter(){
    this.scrollToTopOnInit();
  }

  scrollToTopOnInit(){
    setTimeout(() => {
      if (this.content.scrollToTop) {
          this.content.scrollToTop(400);
      }
  }, 500);
  }

}
