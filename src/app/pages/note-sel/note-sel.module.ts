import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NoteSelPageRoutingModule } from './note-sel-routing.module';

import { NoteSelPage } from './note-sel.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NoteSelPageRoutingModule
  ],
  declarations: [NoteSelPage]
})
export class NoteSelPageModule {}
