import { Component, OnInit,ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-note-sel',
  templateUrl: './note-sel.page.html',
  styleUrls: ['./note-sel.page.scss'],
})
export class NoteSelPage implements OnInit {

  @ViewChild('content',{static:false}) content;

  constructor(private route: Router) { }

  go(){
    this.route.navigate(['add-note-page'])
  }

  ngOnInit() {
    this.scrollToTopOnInit();
  }

  ionViewWillEnter(){
    this.scrollToTopOnInit();
  }

  LISTENANSICHT(){
    this.route.navigateByUrl("tabs/exhibitors-list");
  }

  scrollToTopOnInit(){
    setTimeout(() => {
      if (this.content.scrollToTop) {
          this.content.scrollToTop(400);
      }
  }, 500);
  }

}
 