import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotePageRoutingModule } from './note-routing.module';

import { NotePage } from './note.page';
import { ComponentsModule } from 'src/app/components/components.module';
import {TranslateModule} from '@ngx-translate/core';
import { PipeModule } from 'src/app/pipe.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        NotePageRoutingModule,
        ComponentsModule,
        TranslateModule,
        PipeModule.forRoot()
    ],
    declarations: [NotePage]
})
export class NotePageModule {}
