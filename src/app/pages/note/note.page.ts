import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { AlertController, ModalController } from '@ionic/angular';
import { NoteFormComponent } from 'src/app/components/form/note-form/note-form.component';
import { Note, TYPE_AUDIO, TYPE_IMAGE, TYPE_TEXT } from 'src/app/models/note.model';
import { ProductaddPage } from 'src/app/productadd/productadd.page';
import { ApiService } from 'src/app/services/api-service/api.service';
import { AddAudioNotePagePage } from '../add-audio-note-page/add-audio-note-page.page';
import { AddTextnotePagePage } from '../add-textnote-page/add-textnote-page.page';
import {TranslateService} from '@ngx-translate/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { AppUser } from 'src/app/models/user.model';
import { Notebook } from 'src/app/models/notebook.model';
import { DataService } from 'src/app/services/data/data.service';


@Component({
  selector: 'app-note',
  templateUrl: './note.page.html',
  styleUrls: ['./note.page.scss'],
})
export class NotePage implements OnInit {

  note: Note;
  noteId: number;

  user: AppUser;
  notebook: Notebook;

  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService,
    private location: Location,
    private alertController: AlertController,
    private modalController: ModalController,
    private router: Router,
    private translate: TranslateService,
    private socialSharing: SocialSharing
) { }

  ngOnInit() {
    DataService.noteBook.subscribe(n => {
      this.notebook = n;
    });
    DataService.appUser.subscribe(u => {
      this.user = u;
    });
    this.route.queryParams.subscribe(params => {
      this.noteId = parseInt(this.route.snapshot.paramMap.get('id'));
      this.loadNote();
    });
  }

  isEditable() {
    return this.notebook && this.notebook.isCanEdit(this.user);
  }

  goBack() {
    this.location.back();
  }

  openExhibitor(){
    this.router.navigate(['/tabs/exhibitors-detail', this.note.exhibitor.id]);
  }

  isTextType() {
    return this.note.type === TYPE_TEXT.value;
  }

  isImageType() {
    return this.note.type === TYPE_IMAGE.value;
  }

  isAudioType() {
    return this.note.type === TYPE_AUDIO.value;
  }

  async editNote() {
    let component: any = AddTextnotePagePage;
    if (this.note.product_name) {
      component = ProductaddPage;
    } else if (this.isAudioType()) {
      component = AddAudioNotePagePage;
    }
    const modal = await this.modalController.create({
      component,
      componentProps: {
        note: this.note,
        afterSubmit: () => {
          this.loadNote();
        }
      }
    });
    await modal.present();
  }

  async deleteNote() {
    const alert = await this.alertController.create({
      header: 'Bestätigung löschen',
      message: 'Notiz löschen?',
      buttons: [
        {
          text: 'Nein',
          role: 'cancel',
          cssClass: 'secondary'
        }, {
          text: 'Ja',
          handler: () => {
            this.apiService.deleteNote(this.noteId).then(() => {
              this.goBack();
            });
          }
        }
      ]
    });
    await alert.present();
  }

  private loadNote() {
    this.apiService.getNote(this.noteId).then(data => {
      this.note = data;
    });
  }

  share() {
    this.socialSharing.shareWithOptions({
      message: this.note.toString(),
      subject: this.note.title
      //files: this.note.file ? this.note.file.path : null,
      //url: this.note.file ? this.note.file.path : null
    });
  }

}
