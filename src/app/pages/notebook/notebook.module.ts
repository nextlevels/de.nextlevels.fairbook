import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotebookPageRoutingModule } from './notebook-routing.module';

import { NotebookPage } from './notebook.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotebookPageRoutingModule,
    ComponentsModule
  ],
  declarations: [NotebookPage]
})
export class NotebookPageModule {}
