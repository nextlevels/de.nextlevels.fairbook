import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, ModalController } from '@ionic/angular';
import { NoteFormComponent } from 'src/app/components/form/note-form/note-form.component';
import { NotebookFormComponent } from 'src/app/components/form/notebook-form/notebook-form.component';
import { Note } from 'src/app/models/note.model';
import { Notebook } from 'src/app/models/notebook.model';
import { ApiService } from 'src/app/services/api-service/api.service';

@Component({
  selector: 'app-notebook',
  templateUrl: './notebook.page.html',
  styleUrls: ['./notebook.page.scss'],
})
export class NotebookPage implements OnInit {

  notebook: Notebook;
  notebookId: number;

  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService,
    private location: Location,
    private modalController: ModalController,
    private alertController: AlertController,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.notebookId = parseInt(this.route.snapshot.paramMap.get('id'));
      this.loadNotebook();
    });
  }

  goBack() {
    this.location.back();
  }

  async editNotebook() {
    const modal = await this.modalController.create({
      component: NotebookFormComponent,
      componentProps: {
        notebook: this.notebook,
        afterSubmit: () => {
          this.loadNotebook();
        }
      }
    });
    await modal.present();
  }

  async deleteNotebook() {
    const alert = await this.alertController.create({
      header: 'Bestätigung löschen',
      message: 'Messebuch löschen?',
      buttons: [
        {
          text: 'Nein',
          role: 'cancel',
          cssClass: 'secondary'
        }, {
          text: 'Ja',
          handler: () => {
            this.doDeleteNotebook();
          }
        }
      ]
    });
    await alert.present();
  }

  async newNote() {
    const modal = await this.modalController.create({
      component: NoteFormComponent,
      componentProps: {
        notebook: this.notebook,
        afterSubmit: () => {
          this.loadNotebook();
        }
      }
    });
    await modal.present();
  }

  private doDeleteNotebook() {
    this.apiService.deleteNotebook(this.notebookId).then(() => {
      this.router.navigate(['notebooks']);
    });
  }

  private loadNotebook() {
    this.apiService.getNotebook(this.notebookId).then(data => {
      this.notebook = data;
    });
  }

}
