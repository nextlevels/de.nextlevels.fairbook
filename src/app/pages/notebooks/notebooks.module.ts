import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotebooksPageRoutingModule } from './notebooks-routing.module';

import { NotebooksPage } from './notebooks.page';
import { ComponentsModule } from 'src/app/components/components.module';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotebooksPageRoutingModule,
    ReactiveFormsModule,
    ComponentsModule,
    TranslateModule
  ],
  declarations: [NotebooksPage]
})
export class NotebooksPageModule {}
