import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { NotebookFormComponent } from 'src/app/components/form/notebook-form/notebook-form.component';
import { Notebook } from 'src/app/models/notebook.model';
import { ApiService } from 'src/app/services/api-service/api.service';
import {debounceTime} from 'rxjs/operators';
import {FormControl} from '@angular/forms';
import {DataService} from '../../services/data/data.service';
import {StorageService} from '../../services/storage/storage.service';
import { AppUser } from 'src/app/models/user.model';

@Component({
  selector: 'app-notebooks',
  templateUrl: './notebooks.page.html',
  styleUrls: ['./notebooks.page.scss'],
})
export class NotebooksPage implements OnInit {

  notebooks: Notebook[];
  _filterednotebooks: Notebook[];
  searchTerm = '';
  _filterTeam = false;
  _filterOwn = false;

  searchControl: FormControl;
  searching: any = false;

  user: AppUser;

  constructor(
    private modalController: ModalController,
    private apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private storageService: StorageService,
  ) {
    this.searchControl = new FormControl();
  }

  ngOnInit() {
    this.route.params.subscribe(routeParams => {
      this.loadNotebooks();
    });

    DataService.appUser.subscribe(u => {
      this.user = u;
    });

    this.searchControl.valueChanges
        .pipe(debounceTime(1000))
        .subscribe(search => {
          if (search.length === 0) {
            this.resetFilter();
            this.searching = false;
          } else {
            if (search.length > 2) {
              this.resetFilter();
              this._filterednotebooks = [...this.notebooks].filter(book => {
                const searchString = `${book.title}`;
                return searchString.toLowerCase().indexOf(search.toLowerCase()) > -1;
              });
            }
          }
        });
  }

  onSearchInput() {
    this.searching = true;
  }

  async newNotebook() {
    const modal = await this.modalController.create({
      component: NotebookFormComponent,
      componentProps: {
        afterSubmit: () => {
          this.loadNotebooks();
        }
      }
    });
    await modal.present();
  }

  isOwner(notebook: Notebook) {
    return this.user && this.user.id === notebook.owner_id;
  }

  openNotebook(notebook: Notebook) {
    this.apiService.getNotebook(notebook.id).then(data => {
      this.storageService.set('last_notebook_id', notebook.id).then(() => {});
      DataService.noteBook.next(data);
      this.router.navigate(['/tabs/notes-mixed']);
    });
  }

  private loadNotebooks() {
    this.apiService.getUserNotebooks().then(data => {
      this.notebooks = data;
      this._filterednotebooks = data;
      console.log(data);
    });
  }

  getCountShared() {
    if(this.notebooks !== undefined){
      return this.notebooks.filter(function(book) {
        return book.is_shared ? true : false;
      }).length;
    }
  }

  getCountOwn() {
    if(this.notebooks !== undefined){
    return this.notebooks.filter(function(book) {
      return book.is_shared ? false : true;
    }).length;
    }
  }

  resetFilter() {
    console.log(this.notebooks);
    console.log(this._filterednotebooks);
    this._filterednotebooks = this.notebooks;
  }

  filterShared() {
    if (!this._filterTeam) {
      this._filterednotebooks = this.notebooks.filter(function(book) {
        return book.is_shared == true;
      });
      this._filterTeam = true;
    } else {
      this.resetFilter();
      this._filterTeam = false;
    }
  }

  filterOwn() {
    if (!this._filterOwn) {
      this._filterednotebooks = this.notebooks.filter(function(book) {
        return book.is_shared == false;
      });
      this._filterOwn = true;
    } else {
      this.resetFilter();
      this._filterOwn = false;
    }
  }

}
