import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotesMixedPage } from './notes-mixed.page';

const routes: Routes = [
  {
    path: '',
    component: NotesMixedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotesMixedPageRoutingModule {}
