import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NotesMixedPage } from './notes-mixed.page';

describe('NotesMixedPage', () => {
  let component: NotesMixedPage;
  let fixture: ComponentFixture<NotesMixedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotesMixedPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NotesMixedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
