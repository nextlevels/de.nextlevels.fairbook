import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../services/data/data.service';
import { NoteFormComponent } from '../../components/form/note-form/note-form.component';
import { AlertController, ModalController } from '@ionic/angular';
import { Notebook } from '../../models/notebook.model';
import { ApiService } from '../../services/api-service/api.service';
import {LocalizedDatePipe} from '../../services/language.service';

@Component({
    selector: 'app-notes-mixed',
    templateUrl: './notes-mixed.page.html',
    styleUrls: ['./notes-mixed.page.scss'],
})
export class NotesMixedPage implements OnInit {

    _notebook: Notebook;

    constructor(public router: Router,
                private modalController: ModalController,
                public data: DataService,
                private apiService: ApiService,
                private alertController: AlertController,
                private localizedDate: LocalizedDatePipe
    ) { }

    ngOnInit() {
        DataService.noteBook.subscribe(notebook => {
            this._notebook = notebook;
        });
    }

    ionViewWillEnter() {
        if (this._notebook) {
            this.apiService.getNotebook(this._notebook.id).then(data => {
                DataService.noteBook.next(data);
            });
        }
    }

    async openNotebookDetails() {
        const notebook = DataService.noteBook.value;
        if (notebook) {
            const alert = await this.alertController.create({
                header: notebook.title,
                subHeader: 'Erstellt: ' + this.localizedDate.transform(notebook.created_at),
                message: notebook.description ? notebook.description : '',
                buttons: ['OK']
            });
            await alert.present();
        }
    }

    isEditable() {
        const notebook = DataService.noteBook.value;
        return notebook && notebook.isCanEdit(DataService.appUser.value);
    }

    newNote() {
        DataService.exhibitor_id = null;
        this.router.navigate(['tabs/productadd1']);
    }

    // async newNote() {
    //     const modal = await this.modalController.create({
    //         component: NoteFormComponent,
    //         componentProps: {
    //             notebook: this._notebook,
    //             afterSubmit: () => {
    //                 this.apiService.getNotebook(this._notebook.id).then(data => {
    //                     DataService.noteBook.next(data);
    //                 });
    //             }
    //         }
    //     });
    //     await modal.present();
    // }

}
