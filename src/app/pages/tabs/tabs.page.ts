import { Component } from '@angular/core';

import { ModalController, NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { MenuComponent } from 'src/app/components/modal/menu/menu.component';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  
  constructor(public modalController: ModalController, public navCtrl:NavController,
    public router:Router) {}

  async presentModal() {
    const modal = await this.modalController.create({
      component: MenuComponent,
    });
     await modal.present();
    const { data } = await modal.onWillDismiss();
    if(data){
      if(data=="messe"){
        this.navCtrl.navigateRoot('messebucher');
      }else if(data=="profil"){
        this.router.navigateByUrl('tabs/profil');
      }else if(data=="einstel"){
        this.router.navigateByUrl('tabs/adjustments');
      }else if(data=="datenschutz"){
        this.router.navigateByUrl('tabs/iframe/datenschutz');
      }else if(data=="impressum"){
        this.router.navigateByUrl('tabs/iframe/impressum');
      }else {
        this.navCtrl.navigateRoot('login');
      }
    }else{
     
    }

  }

  goToErfassen(){
    this.router.navigateByUrl('tabs/erfassen2');
  }

  goToHome(){
    this.router.navigateByUrl('tabs/home');
  }

  goToExhibitorsList(){
    this.router.navigateByUrl('tabs/exhibitors-list');
  }

  goToDashboard(){
    this.router.navigateByUrl('tabs/katalog');
  }

}
