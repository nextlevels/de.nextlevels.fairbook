import { NgModule }      from '@angular/core';
import {LocalizedDatePipe} from './services/language.service';

@NgModule({
    imports:        [],
    declarations:   [LocalizedDatePipe],
    exports:        [LocalizedDatePipe],
    providers: []
})

export class PipeModule {

  static forRoot() {
     return {
         ngModule: PipeModule,
         providers: [LocalizedDatePipe],
     };
  }
} 