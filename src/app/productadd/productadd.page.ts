import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ModalController, Platform } from '@ionic/angular';
import { Attachment } from '../models/attachment.model';
import { DataService } from '../services/data/data.service';
import { Note, TYPE_AUDIO, TYPE_IMAGE, TYPE_TEXT } from '../models/note.model';
import { ApiService } from '../services/api-service/api.service';
import { AddTextnotePagePage } from '../pages/add-textnote-page/add-textnote-page.page';
import { AddAudioNotePagePage } from '../pages/add-audio-note-page/add-audio-note-page.page';

@Component({
  selector: 'app-productadd',
  templateUrl: './productadd.page.html',
  styleUrls: ['./productadd.page.scss'],
})
export class ProductaddPage implements OnInit {

  @Input() afterSubmit: Function;
  @Input() note: Note

  form: FormGroup;

  constructor(
    public route: Router,
    private camera: Camera,
    private location: Location,
    private formBuilder: FormBuilder,
    private modalController: ModalController,
    private apiService: ApiService,
    private platform: Platform
  ) { }

  ngOnInit() {
    this.initForm();
    DataService.noteBook.subscribe(notebook => {
      this.form.patchValue({
        notebook_id: notebook ? notebook.id : null,
        exhibitor_id: DataService.exhibitor_id
      });
    });
    if (this.note) {
      this.form.patchValue({
        id: this.note.id,
        title: this.note.title,
        content: this.note.content,
        type: this.note.type,
        product_name: this.note.product_name,
        article_id: this.note.article_id
      });
    }
  }

  onProductNameChange(event) {
    this.form.patchValue({
      title: event.detail.value
    });
  }

  return() {
    if (this.afterSubmit) {
      this.modalController.dismiss();
    } else {
      this.location.back();
      this.location.back();
    }
  }

  async goTextnote() {
    const modal = await this.modalController.create({
      component: AddTextnotePagePage,
      componentProps: {
        note: this.note,
        onSubmit: (data) => {
          this.form.patchValue({
            type: this.form.value.type ? this.form.value.type : TYPE_TEXT.value,
            title: data.title,
            content: data.content
          });
        }
      }
    });
    await modal.present();
  }

  async goAudionote() {
    const modal = await this.modalController.create({
      component: AddAudioNotePagePage,
      componentProps: {
        note: this.note,
        onSubmit: (data) => {
          this.form.patchValue({
            type: TYPE_AUDIO.value,
            title: data.title,
            file: data.file
          });
        }
      }
    });
    await modal.present();
  }



  openCamera() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    if (this.platform.is('mobileweb')) {
      const input = document.createElement('input');
      const container = document.getElementById('main');
      input.type = "file";
      input.hidden = true;
      container.appendChild(input);
      const reader = new FileReader();
      let filename;
      let size;
      input.onchange = () => {
        let file = input.files[0];
        if (file) {
          filename = file.name;
          size = file.size;
          reader.readAsDataURL(file);
        }
      };
      const comp = this;
      reader.addEventListener("load", function () {
        container.removeChild(input);
        const title = `foto-${new Date().getTime()}`;
        console.log(comp.form.value);
        comp.form.patchValue({
          type: TYPE_IMAGE.value,
          file: new Attachment({
            file_name: title + '.jpg',
            data: reader.result,
            file_size: size
          }),
          title: title,
        })
      }, false);
      input.click();
    } else {
      this.camera.getPicture(options).then((imageData) => {
        const title = `foto-${new Date().getTime()}`;
        this.form.patchValue({
          type: TYPE_IMAGE.value,
          file: new Attachment({
            data: 'data:image/jpeg;base64,' + imageData,
            file_name: title + '.jpg'
          }),
          title: title,
        })
      }, (err) => {
        console.log(err);
        // Handle error
      });
    }

  }

  submit() {
    const data = this.form.value;
    if (data.id) {
      this.apiService.updateNote(data).then(() => {
        if (this.afterSubmit) {
          this.afterSubmit();
        }
        this.return();
      });
    } else {
      this.apiService.createNote(data).then(() => {
        if (this.afterSubmit) {
          this.afterSubmit();
        }
        this.return();
      });
    }
  }

  private initForm() {
    this.form = this.formBuilder.group({
      id: new FormControl(null, []),
      title: new FormControl(null, [Validators.required, Validators.maxLength(65535)]),
      content: new FormControl(null, [Validators.maxLength(65535)]),
      file: new FormControl(null, []),
      type: new FormControl(TYPE_TEXT.value, [Validators.required]),
      notebook_id: new FormControl(null, [Validators.required]),
      exhibitor_id: new FormControl(null, []),
      product_name: new FormControl(null, [Validators.required, Validators.maxLength(255)]),
      article_id: new FormControl(null, [Validators.maxLength(255)])
    });
  }

}
