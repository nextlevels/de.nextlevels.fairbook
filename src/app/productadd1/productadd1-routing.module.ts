import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Productadd1Page } from './productadd1.page';

const routes: Routes = [
  {
    path: '',
    component: Productadd1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Productadd1PageRoutingModule {}
