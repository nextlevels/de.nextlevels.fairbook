import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Productadd1PageRoutingModule } from './productadd1-routing.module';

import { Productadd1Page } from './productadd1.page';
import { ComponentsModule } from '../components/components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Productadd1PageRoutingModule,
    ComponentsModule,
    TranslateModule
  ],
  declarations: [Productadd1Page]
})
export class Productadd1PageModule {}
