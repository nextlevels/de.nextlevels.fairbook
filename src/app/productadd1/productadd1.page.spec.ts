import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Productadd1Page } from './productadd1.page';

describe('Productadd1Page', () => {
  let component: Productadd1Page;
  let fixture: ComponentFixture<Productadd1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Productadd1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Productadd1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
