import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { AlertController, NavController } from '@ionic/angular';
import { Attachment } from '../models/attachment.model';
import { TYPE_IMAGE } from '../models/note.model';
import { ApiService } from '../services/api-service/api.service';
import { DataService } from '../services/data/data.service';

@Component({
  selector: 'app-productadd1',
  templateUrl: './productadd1.page.html',
  styleUrls: ['./productadd1.page.scss'],
})
export class Productadd1Page implements OnInit {

  notebook;


  constructor(
    public route: Router,
    private apiService: ApiService,
    private camera: Camera,
    private navCtrl: NavController,
    private alertController: AlertController,
    private location: Location
  ) { }

  ngOnInit() {
    DataService.noteBook.subscribe(n => {
      this.notebook = n;
    });
  }


  goTextnote() {

    this.route.navigate(['tabs/add-textnote-page']);
  }

  goAudionote() {

    this.route.navigate(['tabs/add-audio-note-page']);
  }
  goToProduct() {
    this.route.navigate(['tabs/productadd']);
  }

  back() {
    this.navCtrl.back();
  }



  openCamera() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      const title = `foto-${new Date().getTime()}`;
      const note = {
        id: null,
        type: TYPE_IMAGE.value,
        file: new Attachment({
          data: 'data:image/jpeg;base64,' + imageData,
          file_name: title + '.jpg'
        }),
        title: title,
        notebook_id: this.notebook.id,
        exhibitor_id: DataService.exhibitor_id
      };
      this.doCreatePhoto(note);
    }, (err) => {
      console.log(err);
    });
  }

  async doCreatePhoto(note) {
    const alert = await this.alertController.create({
      header: 'Titel des Fotos (optional)',
      inputs: [
        {
          name: 'title',
          placeholder: 'Title'
        }
      ],
      buttons: [
        {
          text: 'Notiz erstellen',
          handler: (data) => {
            if (data.title) {
              note.title = data.title;
            }
            this.apiService.createNote(note).then(() => {
              this.apiService.getNotebook(this.notebook.id).then(n => {
                DataService.noteBook.next(n);
                this.location.back();
              });
            });
          }
        }
      ]
    });
    await alert.present();
  }

}
