import { Injectable } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';
import { Exhibition } from 'src/app/models/exhibition.model';
import { Note } from 'src/app/models/note.model';
import { Notebook } from 'src/app/models/notebook.model';
import { AppUser, Principal } from 'src/app/models/user.model';
import { HttpService } from '../http-service/http.service';
import m_Exhibitor from '../../models/exhibitors.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpService,
    private loadingController: LoadingController,
    private toast: ToastController
  ) { }
  // ========================================================== USERS =================================================================

  loginUser(username: string, password: string): Promise<Principal> {
    return new Promise<Principal>((resolve) => {
      this.showLoader().then(l => {
        l.present();
        const params = {
          email: username,
          password
        };
        return this.http.post('/api/user/login', params, l).subscribe(data => {
          l.dismiss();
          resolve(new Principal(data));
        });
      });
    });
  }

  updateToken(): Promise<Principal> {
    return new Promise<Principal>(resolve => {
      this.http.post('/api/refresh').subscribe(data => {
        resolve(new Principal(data));
      });
    });
  }

  registerNewUser(data: any): Promise<any> {
    return new Promise<any>((resolve) => {
      this.showLoader().then(l => {
        l.present();
        return this.http.post('/api/user/register', data, l).subscribe(() => {
          l.dismiss();
          resolve();
        });
      });
    });
  }

  loginViaGoogle(data: any): Promise<any> {
    return new Promise<any>(resolve => {
      this.showLoader().then(l => {
        l.present();
        return this.http.post('/api/user/google-login', data, l).subscribe(() => {
          l.dismiss();
          resolve();
        });
      });
    });
  }

  loginViaApple(data: any): Promise<any> {
    return new Promise<any>(resolve => {
      this.showLoader().then(l => {
        l.present();
        return this.http.post('/api/user/apple-login', data, l).subscribe(() => {
          l.dismiss();
          resolve();
        });
      });
    });
  }

  getAppUser(): Promise<AppUser> {
    return new Promise<any>(resolve => {
      return this.http.get('/api/user/app').subscribe(data => {
        resolve(new AppUser(data));
      });
    });
  }

  updateUserProfile(data: any): Promise<AppUser> {
    return new Promise<AppUser>(resolve => {
      this.showLoader().then(l => {
        l.present();
        return this.http.put('/api/user/update', data, l).subscribe(data => {
          l.dismiss();
          resolve(new AppUser(data));
        });
      });
    });
  }

  forgotPassword(email: string): Promise<any> {
    return new Promise<any>(resolve => {
      this.showLoader().then(l => {
        l.present();
        return this.http.post('/api/user/forgot-password', { email }, l).subscribe(data => {
          l.dismiss();
          resolve();
        });
      });
    });
  }

  changePassword(data: any): Promise<any> {
    return new Promise<any>(resolve => {
      this.showLoader().then(l => {
        l.present();
        return this.http.post('/api/user/change-password', data, l).subscribe(data => {
          l.dismiss();
          resolve();
        });
      });
    });
  }

  searchUsers(payload: any): Promise<AppUser[]> {
    return new Promise<AppUser[]>(resolve => {
      return this.http.post('/api/user/search', payload).subscribe(data => {
        const result: AppUser[] = [];
        data.forEach(item => {
          result.push(new AppUser(item));
        });
        resolve(result);
      });
    });
  }

  // =========================================================== EXHIBITIONS =========================================================

  getAllExhibitions(): Promise<Exhibition[]> {
    return new Promise<Exhibition[]>(resolve => {
      return this.http.get('/api/exhibitions').subscribe(data => {
        const result: Exhibition[] = [];
        data.forEach(item => {
          result.push(new Exhibition(item));
        });
        resolve(result);
      });
    });
  }

  // =========================================================== EXHIBITIORS =========================================================

  getAllExhibitiors(id): Promise<m_Exhibitor[]> {
    return new Promise<m_Exhibitor[]>(resolve => {
      return this.http.get('/api/exhibitions/' + id + '/exhibitors').subscribe(data => {
        const result: m_Exhibitor[] = [];
        data.forEach(item => {
          result.push(item as m_Exhibitor);
        });
        resolve(result);
      });
    });
  }

  getAllExhibitiorsUser(id): Promise<m_Exhibitor[]> {
    return new Promise<m_Exhibitor[]>(resolve => {
      return this.http.get('/api/user/exhibitions/' + id + '/exhibitors').subscribe(data => {
        const result: m_Exhibitor[] = [];
        data.forEach(item => {
          result.push(item as m_Exhibitor);
        });
        resolve(result);
      });
    });
  }

  getExhibitorById(id: number): Promise<m_Exhibitor> {
    return new Promise<m_Exhibitor>(resolve => {
      return this.http.get('/api/exhibitors/' + id).subscribe(data => {
        resolve(data as m_Exhibitor);
      });
    });
  }

  addExhibitor(id: number): Promise<any> {
    return new Promise<any>(resolve => {
        return this.http.post('/api/user/exhibitors/' + id + '/add').subscribe(data => {
          resolve();
        });
    });
  }

  bookMarkExhibitor(id: number): Promise<any> {
    return new Promise<any>(resolve => {
      return this.http.post('/api/user/exhibitors/' + id + '/toggle-bookmark').subscribe(data => {
        resolve();
      });
    });
  }

  searchByStandNumber(standNumber): Promise<m_Exhibitor> {
    return new Promise<m_Exhibitor>(resolve => {
      this.showLoader().then(l => {
        l.present();
        return this.http.get('/api/user/exhibitors/search/' + standNumber, null, l).subscribe(data => {
          l.dismiss();
          resolve(data as m_Exhibitor);
        });
      });
    });
  }

  scanBusinessCard(image: string, exhibitionId: number): Promise<m_Exhibitor> {
    return new Promise<m_Exhibitor>(resolve => {
      this.showLoader().then(l => {
        l.present();
        return this.http.post('/api/exhibitors/scan', { business_card: image, exhibition_id : exhibitionId }, l).subscribe(data => {
          l.dismiss();
          resolve(data as m_Exhibitor);
        });
      });
    });
  }

  // =========================================================== NOTEBOOKS ===========================================================

  createNotebook(data: any): Promise<any> {
    return new Promise<any>(resolve => {
      this.showLoader().then(l => {
        l.present();
        return this.http.post('/api/user/notebooks/add', data, l).subscribe(data => {
          l.dismiss();
          resolve();
        });
      });
    });
  }

  updateNotebook(data: any): Promise<any> {
    return new Promise<any>(resolve => {
      this.showLoader().then(l => {
        l.present();
        return this.http.put('/api/user/notebooks/update', data, l).subscribe(data => {
          l.dismiss();
          resolve();
        });
      });
    });
  }

  getUserNotebooks(): Promise<Notebook[]> {
    return new Promise<Notebook[]>(resolve => {
      return this.http.get('/api/user/notebooks').subscribe(data => {
        const result: Notebook[] = [];
        data.forEach(item => {
          result.push(new Notebook(item));
        });
        resolve(result);
      });
    });
  }

  getNotebook(id: number): Promise<Notebook> {
    return new Promise<Notebook>(resolve => {
      return this.http.get('/api/user/notebooks/' + id).subscribe(data => {
        resolve(new Notebook(data));
      });
    });
  }

  deleteNotebook(id: number): Promise<any> {
    return new Promise<any>(resolve => {
      this.showLoader().then(l => {
        l.present();
        return this.http.delete('/api/user/notebooks/' + id, null, l).subscribe(data => {
          l.dismiss();
          resolve();
        });
      });
    });
  }

  // =========================================================== NOTES ===============================================================

  createNote(data: any): Promise<any> {
    return new Promise<any>(resolve => {
      this.showLoader().then(l => {
        l.present();
        return this.http.post('/api/user/notebooks/add-note', data, l).subscribe(data => {
          l.dismiss();
          resolve();
          this.presentToast('Erfolgreich gespeichert!');
        });
      });
    });
  }

  getNote(id: number): Promise<Note> {
    return new Promise<Note>(resolve => {
      return this.http.get('/api/user/notes/' + id).subscribe(data => {
        resolve(new Note(data));
      });
    });
  }

  deleteNote(id: number): Promise<any> {
    return new Promise<any>(resolve => {
      this.showLoader().then(l => {
        l.present();
        return this.http.delete('/api/user/notes/' + id, null, l).subscribe(data => {
          l.dismiss();
          resolve();
        });
      });
    });
  }

  updateNote(data: any): Promise<any> {
    return new Promise<any>(resolve => {
      this.showLoader().then(l => {
        l.present();
        return this.http.put('/api/user/notes', data, l).subscribe(data => {
          l.dismiss();
          resolve();
        });
      });
    });
  }

  // ----------------------------------------------------------- PRIVATE -------------------------------------------------------------
  private showLoader() {
    return this.loadingController.create({
      spinner: 'circular',
      showBackdrop: true,
      cssClass: 'app-spinner-class',
      animated: true,
      keyboardClose: true
    });
  }

  private async presentToast(message: string, color: string = 'success', duration: number = 2000) {
    const toast = await this.toast.create({
      message,
      duration,
      color
    });
    toast.present();
  }
}
