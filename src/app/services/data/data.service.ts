import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import m_Book from '../../models/book.model';
import {AppUser, Principal, User} from '../../models/user.model';
import DummyBooks from '../../../assets/dummy/Books.dummy';
import m_Note from '../../models/notes.model';
import DummyNotes from '../../../assets/dummy/Notes.dummy';
import { _BASE_URL_ } from 'src/environments/environment';
import { StorageService } from '../storage/storage.service';
import { Router } from '@angular/router';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import m_Exhibitor from '../../models/exhibitors.model';
import {Notebook} from '../../models/notebook.model';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    static host: string = _BASE_URL_;
    /** User principal aka October CMS user. */
    static principal: BehaviorSubject<Principal> = new BehaviorSubject<Principal>(null);
    /** App user details. */
    static appUser: BehaviorSubject<AppUser> = new BehaviorSubject<AppUser>(null);
    /** Last HTTP error details. */
    static lastError: BehaviorSubject<any> = new BehaviorSubject<any>(null);

    static noteBook: BehaviorSubject<Notebook> = new BehaviorSubject<Notebook>(null);
    /** Exhibitor ID, is used for save state between stateless pages. */
    static exhibitor_id: number;

    constructor(
        private storageService: StorageService,
        private router: Router,
        private googlePlus: GooglePlus
    ) {

    }

    logout() {
        let asyncTasks = [];
        if (DataService.appUser.value && DataService.appUser.value.google_user_id) {
            asyncTasks.push(this.googlePlus.logout());
        }
        asyncTasks.push(this.storageService.delete('fairbook_token'));
        Promise.all(asyncTasks).then(() => {
            DataService.principal.next(null);
            DataService.appUser.next(null);
            this.router.navigate(['/login']);
        });
    }


    static _user: User;

    activeExhibitor: m_Exhibitor = null;

     books = new BehaviorSubject<m_Book[]>(null);
     notes = new BehaviorSubject<m_Note[]>(null);
     exhibitors = new BehaviorSubject<m_Exhibitor[]>(null);

     destroy(){
         this.books = new BehaviorSubject<m_Book[]>(null);
     }

}

