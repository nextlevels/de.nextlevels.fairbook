import { Injectable } from '@angular/core';
import { DirectoryEntry, FileEntry, File, Entry } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Platform } from '@ionic/angular';
import { Attachment } from 'src/app/models/attachment.model';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { IOSFilePicker } from '@ionic-native/file-picker/ngx';

export function getFileReader(): FileReader {
  const fileReader = new FileReader();
  const zoneOriginalInstance = (fileReader as any)["__zone_symbol__originalInstance"];
  return zoneOriginalInstance || fileReader;
}

const _APP_FOLDER = 'fairbook';


@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(
    private fileSystem: File,
    private filePath: FilePath,
    private platform: Platform,
    private iosFilePicker: IOSFilePicker,
    private fileChooser: FileChooser
  ) { }

  public selectFileFromDevice(mime: string = null): Promise<Attachment> {
    return new Promise((resolve) => {
      if (this.platform.is('mobileweb')) {
        const input = document.createElement('input');
        const container = document.getElementById('main');
        input.type = "file";
        input.hidden = true;
        container.appendChild(input);
        const reader = new FileReader();
        let filename;
        let size;
        input.onchange = () => {
          let file = input.files[0];
          if (file) {
            filename = file.name;
            size = file.size;
            reader.readAsDataURL(file);
          }
        };
        reader.addEventListener("load", function () {
          container.removeChild(input);
          resolve(new Attachment({
            file_name: filename,
            data: reader.result,
            file_size: size
          }));
        }, false);
        input.click();
      } else if (this.platform.is('android')) {
        let options: any = {};
        if (mime) {
          options.mime = mime;
        }
        this.fileChooser.open(options)
          .then(uri => {
            this.readURI(uri).then((data) => {
              resolve(new Attachment({
                file_name: uri.substring(uri.lastIndexOf('/') + 1),
                data: data,
                file_size: 0
              }));
            });
          })
          .catch(e => console.log(JSON.stringify(e)));
      } else if (this.platform.is('ios')) {
        this.iosFilePicker.pickFile().then(uri => {
          this.readURI(uri).then((data) => {
            resolve(new Attachment({
              file_name: uri.substring(uri.lastIndexOf('/') + 1),
              data: data,
              file_size: 0
            }));
          });
        }).catch(e => JSON.stringify(e));
      }
    });
  }

  public deleteFile(file: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getFile(file).then(f => {
        f.remove(() => {
          resolve();
        }, (e) => {
          console.error(JSON.stringify(e));
          resolve();
        });
      });
    });
  }

  public deleteFileByPrefix(dir: string, prefix: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.fileSystem.listDir(this.fileSystem.dataDirectory, _APP_FOLDER + '/' + dir).then((result: Entry[]) => {
        const promises = [];
        result.forEach(e => {
          if (e.isFile && e.name.indexOf(prefix) === 0) {
            promises.push(new Promise((r, rj) => {
              e.remove(() => {
                r();
              }, (e) => {
                console.log(JSON.stringify(e));
                r();
              })
            }));
          }
        });
        Promise.all(promises).then(() => {
          resolve();
        });
      }).catch(e => {
        console.log(JSON.stringify(e));
        resolve();
      });
    });
  }

  public writeToFile(file: string, content: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getFile(file).then(f => {
        f.createWriter((fw) => {
          fw.onwriteend = function () {
            //console.log('file write content - completed...');
            resolve();
          };
          fw.onerror = () => {
            console.error('can not write file: ' + file);
          };
          fw.write(content);
        });
      });
    });
  }

  public readFromFile(file: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.platform.ready().then(() => {
        this.getFile(file).then((f) => {
          f.file((fileEntry) => {
            var reader = getFileReader();
            reader.onloadend = (evt) => {
              resolve(evt.target.result);
            };
            reader.readAsText(fileEntry);
          }, (err) => {
            console.error('can not read file: ' + file);
          });
        });
      });
    });
  }

  public readURI(uri: string): Promise<any> {
    console.log('URI: ' + uri);
    return new Promise((resolve, reject) => {
      this.platform.ready().then(async () => {
        if (this.platform.is('android')) {
          this.filePath.resolveNativePath(uri).then(async uri => {
            console.log('URI -> ' + uri);
            const folder = uri.substring(0, uri.lastIndexOf('/'));
            const filename = uri.substring(uri.lastIndexOf('/') + 1);
            console.log('FOLDER: ' + folder);
            console.log('FILE: ' + filename);
            const entry: DirectoryEntry = await this.fileSystem.resolveDirectoryUrl(folder);
            console.log(JSON.stringify(entry));
            const f: FileEntry = await this.fileSystem.getFile(entry, filename, { create: false, exclusive: false });
            console.log(JSON.stringify(f));
            f.file((fileEntry) => {
              var reader = getFileReader();
              reader.onloadend = (evt) => {
                resolve(evt.target.result);
              };
              reader.onerror = (e) => {
                console.log('read URI error: ' + JSON.stringify(e));
              }
              reader.readAsDataURL(fileEntry);
            }, (err) => {
              console.error('can not read file: ' + f);
            });
          });
        } else if (this.platform.is('ios')) {
          //console.log("uri:"+uri);
          if (uri.substring(0,7)!=="file://"){
            uri = "file://"+uri;
          }
          
          const folder = uri.substring(0, uri.lastIndexOf('/'));
          //console.log("folder:"+folder);
          const filename = uri.substring(uri.lastIndexOf('/') + 1);
          //console.log("filename:"+filename);
          const entry: DirectoryEntry = await this.fileSystem.resolveDirectoryUrl(folder);
          //console.log("DirectoryEntry:"+JSON.stringify(entry));
          const f: FileEntry = await this.fileSystem.getFile(entry, filename, { create: false, exclusive: false });
          //console.log("FileEntry:"+JSON.stringify(f));
          f.file((fileEntry) => {
            var reader = getFileReader();
            reader.onloadend = (evt) => {
              resolve(evt.target.result);
            };
            reader.onerror = (e) => {
              console.log('read URI error: ' + JSON.stringify(e));
            }
            reader.readAsDataURL(fileEntry);
          }, (err) => {
            console.error('can not read file: ' + f);
          });
        }
      });
    });
  }

  private getFile(file: string): Promise<FileEntry> {
    const filename = file.substring(file.lastIndexOf('/') + 1);
    const dir = file.substring(0, file.lastIndexOf('/'));
    return new Promise((resolve, reject) => {
      this.getDir(dir).then((dirEntry: DirectoryEntry) => {
        this.fileSystem.getFile(dirEntry, filename, { create: true, exclusive: false }).then(f => {
          resolve(f);
        });
      });
    });
  }

  private getDir(dir: string): Promise<DirectoryEntry> {
    const dirFullPath = _APP_FOLDER + dir;
    return new Promise((resolve, reject) => {
      this.platform.ready().then(async () => {
        const subDirs = dirFullPath.split('/');
        let parentDir: DirectoryEntry = await this.fileSystem.resolveDirectoryUrl(this.fileSystem.dataDirectory);
        for (let i = 0; i < subDirs.length; i++) {
          let subDir = subDirs[i];
          let promise: Promise<DirectoryEntry> = this.createSubDir(parentDir, subDir);
          parentDir = await promise;
        }
        // console.log('====================');
        // console.log(JSON.stringify(parentDir));
        // console.log('====================');
        resolve(parentDir);
      });
    });
  }

  private createSubDir(parentDir: DirectoryEntry, subDir: string): Promise<DirectoryEntry> {
    return new Promise(resolve => {
      this.fileSystem.getDirectory(parentDir, subDir, {
        create: true, exclusive: false
      }).then((dir) => {
        resolve(dir);
      })
    })
  }
}
