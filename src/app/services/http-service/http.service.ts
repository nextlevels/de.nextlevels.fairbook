import { Injectable } from '@angular/core';
import { Observable, from, throwError } from 'rxjs';
import { Platform } from '@ionic/angular';
import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { DataService } from "../data/data.service";
import { HTTP } from '@ionic-native/http/ngx';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/services/shared/shared.service';

@Injectable({
    providedIn: 'root'
})
export class HttpService {

    constructor(
        private http: HttpClient,
        private platform: Platform,
        private httpNative: HTTP,
        private router: Router,
        private sharedService: SharedService
    ) {
        this.httpNative.setDataSerializer('utf8');
    }

    get(endpoint: string, params: any = null, loader: any = null): Observable<any> {
        const token = DataService.principal.value ? DataService.principal.value.token : null;
        const headers: any = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': '*',
            'Accept': '*',
        };
        if (token) { 
            headers['Authorization'] = 'Bearer ' + token;
        }
        if (this.platform.is('cordova')) {
            return from(this.httpNative.get(DataService.host + endpoint, params, headers)).pipe(map(data => {
                const resp = <any>JSON.parse(data.data);
                return resp.response;
            }), catchError(x => this.handleError(x, loader) ));
        } else {
            return this.http.get(DataService.host + endpoint, { params: params, headers: headers }).pipe(map(data => {
                const resp = <any>data;
                return resp.response;
            }), catchError(x => this.handleError(x, loader) ));
        }
    }

    delete(endpoint: string, params: any = null, loader: any = null): Observable<any> {
        const token = DataService.principal.value ? DataService.principal.value.token : null;
        const headers: any = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': '*',
            'Accept': '*',
        };
        if (token) {
            headers['Authorization'] = 'Bearer ' + token;
        }
        if (this.platform.is('cordova')) {
            return from(this.httpNative.delete(DataService.host + endpoint, params, headers)).pipe(map(data => {
                const resp = <any>JSON.parse(data.data);
                return resp.response;
            }), catchError(x => this.handleError(x, loader) ));
        } else {
            return this.http.delete(DataService.host + endpoint, { params: params, headers: headers }).pipe(map(data => {
                const resp = <any>data;
                return resp.response;
            }), catchError(x => this.handleError(x, loader) ));
        }
    }

    post(endpoint: string, params: any = null, loader: any = null): Observable<any> {
        const headers: any = {
            'Accept': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json'
        };
        const token = DataService.principal.value ? DataService.principal.value.token : null;
        if (token) {
            headers['Authorization'] = 'Bearer ' + token;
        }
        if (this.platform.is('cordova')) {
            return from(this.httpNative.post(DataService.host + endpoint, this.getHttpParams(params), headers)).pipe(map(data => {
                return <any>JSON.parse(data.data).response;
            }), catchError(x => this.handleError(x, loader) ));
        } else {
            return this.http.post(DataService.host + endpoint, params, { headers: headers }).pipe(map(data => {
                const resp = <any>data;
                return resp.response;
            }), catchError(x => this.handleError(x, loader) ));
        }
    }

    put(endpoint: string, params: any = null, loader: any = null): Observable<any> {
        const headers: any = {
            'Accept': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json'
        };
        const token = DataService.principal.value ? DataService.principal.value.token : null;
        if (token) {
            headers['Authorization'] = 'Bearer ' + token;
        }
        if (this.platform.is('cordova')) {
            return from(this.httpNative.put(DataService.host + endpoint, this.getHttpParams(params), headers)).pipe(map(data => {
                return <any>JSON.parse(data.data);
            }), catchError(x => this.handleError(x, loader) ));
        } else {
            return this.http.put(DataService.host + endpoint, params, { headers: headers }).pipe(map(data => {
                const resp = <any>data;
                return resp.response;
            }), catchError(x => this.handleError(x, loader) ));
        }
    }
    
    handleError(err: HttpErrorResponse, loader: any = null) {
        console.log(JSON.stringify(err));
        DataService.lastError.next(err);
        if (loader) {
            loader.dismiss();
        }
        console.log('HTTP error status: ' + err.status);
        if (err.status === 401) {
            this.router.navigate(['login']);
        } else if (err.status === 500) {
            let errMessage = 'Ein unbekannter Fehler ist aufgetreten.';
            let errTitle = 'Fehler!';
            if (err.error && err.error.errorMessage) {
                errMessage = err.error.errorMessage;
            } else if (err.error && 'string' === typeof err.error) {
                try {
                    const errObj = JSON.parse(err.error);
                    errMessage = errObj.errorMessage;
                } catch (e) {
                    console.log(JSON.stringify(e));
                }
            }
            if (errMessage === 'Deine E-Mailadresse wird bereits benutzt.') {
                errTitle = 'Hoppla!';
            } else if (errMessage === 'Benutzerkonto ist noch nicht aktiviert') {
                errTitle = 'Ups!';
                errMessage = 'Dein Account ist noch nicht bestätigt.';
            }
            console.log('Displayed error message: ' + errMessage);
            this.sharedService.simpleAlert(errTitle, errMessage);
        } else if (err.status === 404) {
            this.sharedService.simpleAlert('Fehler!', 'Seite nicht gefunden.');
        } else if (err.status <= 0) {
            this.sharedService.simpleAlert('Fehler!', 'Sie haben schwaches bis kein Internet');
        }
        return throwError(err);
    }

    getHttpParams(params) {
        if (params) {
            if (params instanceof Object || params instanceof Array) {
                return JSON.stringify(params);
            } else {
                return params;
            }
        } else {
            return '';
        }
    }
}
