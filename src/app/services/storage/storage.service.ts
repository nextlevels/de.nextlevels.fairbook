import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { FileService } from '../file-service/file.service';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(
    private platform: Platform,
    private fileService: FileService
  ) {}


  set(key: string, object: any): Promise<any> {
    return new Promise((resolve, reject) => {
      if (this.platform.is("cordova")) {
        this.fileService.writeToFile('/settings/' + key, object).then(() => {
          resolve();
        });
      } else {
        localStorage.setItem(key, object instanceof Object ? JSON.stringify(object) : object);
        resolve();
      }
    });
  }

  delete(key: string): Promise<any> {
    return new Promise((resolve, reject) => {
      if (this.platform.is("cordova")) {
        this.fileService.deleteFile('/settings/' + key).then(() => {
          resolve();
        });
      } else {
        localStorage.removeItem(key);
        resolve();
      }
    });
  }

  deleteByPrefix(prefix: string): Promise<any> {
    const promises = [];
    if (this.platform.is("cordova")) {
      promises.push(this.fileService.deleteFileByPrefix('settings', prefix));
    } else {
      for (var key in localStorage) {
        if (key.indexOf(prefix) === 0) {
          promises.push(this.delete(key));
        }
      }
    }
    return Promise.all(promises);
  }

  get(key: string): Promise<any> {
    return new Promise((resolve, reject) => {
      if (this.platform.is("cordova")) {
        this.fileService.readFromFile('/settings/' + key).then((val) => {
          resolve(val);
        });
      } else {
        resolve(localStorage.getItem(key));
      }
    });

  }
}
