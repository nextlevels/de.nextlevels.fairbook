import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-social-sharing',
  templateUrl: './social-sharing.page.html',
  styleUrls: ['./social-sharing.page.scss'],
})
export class SocialSharingPage implements OnInit {

  constructor(public modalController: ModalController,private socialSharing: SocialSharing) { }

  ngOnInit() {
  }

  closeModal(){
    this.modalController.dismiss();
  }

  
facebookShare(){
  var msg  = 'hey there!';
   this.socialSharing.shareViaFacebook(msg, null, null).then((success) =>{
    alert("Success");
})
.catch((err)=>{
   alert("Could not share information");
 });
 }

 twitterShare(){
  var msg  = 'hey there!';
  this.socialSharing.shareViaTwitter(msg, null, null).then((success) =>{
    alert("Success");
})
.catch((err)=>{
   alert("Could not share information");
 });
}

whatsappShare(){
  var msg  = 'hey there!';
   this.socialSharing.shareViaWhatsApp(msg, null, null).then((success) =>{
    alert("Success");
})
.catch((err)=>{
   alert("Could not share information");
 });
 }

 instagramShare(){
  var msg  = 'hey there!';
   this.socialSharing.shareViaInstagram(msg, null).then((success) =>{
    alert("Success");
})
.catch((err)=>{
   alert("Could not share information");
 });
 }

//  emailShare(){
//   this.socialSharing.shareViaEmail('Body', 'Subject', ['recipient@example.org']).then(() => {
//     // Success!
//   }).catch(() => {
//     // Error!
//   });
//  }
 
}
