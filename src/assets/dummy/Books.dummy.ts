import m_Book from '../../app/models/book.model';


let DummyBooks: m_Book[] = [
    new m_Book(1, 'Die Flotte 17.07.2020', true, false, 'Flotte'),
    new m_Book(2, 'Honkong Messe 17.03.2020', true, true, 'Flotte'),
    new m_Book(3, 'IAA 23.02.2020', true, false, 'Flotte'),
    new m_Book(4, 'PSI 20.01.2020', true, true, 'Flotte'),
    new m_Book(5, 'Cebit 12.12.2019', true, true, 'Flotte'),
    new m_Book(5, 'IFA 13.10.2019', true, true, 'Flotte'),
];



export default DummyBooks;
