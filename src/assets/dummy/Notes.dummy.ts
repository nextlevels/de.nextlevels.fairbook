import m_Note from '../../app/models/notes.model';


let DummyNotes: m_Note[] = [
    new m_Note(1, 'Kapselheber', 'Kapselheber in verschiedenen Farben und ab 100 Stück personalisierbar. Sonderkonditionen ab Bestellmenge 10.000 Stück','text', '', '',1),
    new m_Note(1, 'Zahlungskonditionen', 'Immer Vorkasse; Michael Meier meinte zu mir, dass man Ausnahmen vereinbaren kann','text', '', '',1),
    new m_Note(3, 'Cooler Messestand', '','image', 'image/cooler_messestand.jpg', 'Kapselheber',2),

    new m_Note(1, 'Ansprechpartner', 'AP ist Benjamin Ziege; Rufnummer 0151 22232233','text', '', '',1),
    new m_Note(2, 'Guter Werbeslogan', '','voice', 'voice/Guter Werbeslogan.m4a', 'Kapselheber',3),
    new m_Note(3, 'Fußball Qualität', '','image', 'image/fußball.jpg', 'Kapselheber',1),
    new m_Note(3, 'Farben Kugelschreiber', '','image', 'image/kullis.jpg', 'Kapselheber',3),
    new m_Note(1, 'Sortiment', 'Hat über 4 andere Firmen noch weitere Artikel im Bestand. Restposten durch Aufkauf. ','text', '', '',1),
    new m_Note(2, 'Kontaktdaten', '','voice', 'voice/Kontaktdaten.m4a', 'Kapselheber',1),
    new m_Note(2, 'Gute Telefonstimme', '','voice', 'voice/Gute Telefonstimme.m4a', 'Kapselheber',1),
    new m_Note(2, 'Guter Promoter', '','voice', 'voice/Guter Promoter.m4a', 'Kapselheber',2),
    new m_Note(2, 'Sound Musikbox', '','voice', 'voice/Sound Musikbox.m4a', 'Kapselheber',2),

    new m_Note(3, 'Günstige weiße Tube', '','image', 'image/weiße_tube.jpg', 'Kapselheber',1),
    new m_Note(1, 'Lieferzeiten', 'Lieferzeiten bei diesem Lieferant 1-2 Werktage unter 1.000er Bestellmenge. Über 1.000er Bestellmenge 5-7 Werktagek','text', '', '',1),

];



export default DummyNotes;
